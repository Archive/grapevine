/* Grapevine: GNOME Notifications
 *   Test client
 * Author: George Lebl
 * (c) 2000 Eazel, Inc.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.	 See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#include "config.h"
#include <gnome.h>
#include <bonobo.h>
#include <liboaf/liboaf.h>

#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <stdlib.h>
#include <time.h>

#include "grapevine-i18n.h"

#include "grapevine.h"

static GList *progress_notices = NULL;

static GtkWidget *app = NULL;

static gboolean
add_simple(gpointer data)
{
	GrapevineNotification *client = data;
	char *uniqueID;
	char *keywords[] = {
		"Foo",
		"Bar",
		NULL
	};
	int i;

	i = rand()%4;
	switch(i) {
	case 0:
		grapevine_notification_post_simple_notice(client, NULL,
						    GRAPEVINE_INFORMATIONAL,
						    keywords,
						    "Simple summary",
						    "Nothing really "
						    "happened!",
						    15,
						    &uniqueID);
		break;
	case 1:
		grapevine_notification_post_simple_notice(client, NULL,
						    GRAPEVINE_CRITICAL,
						    keywords,
						    "Martians invaded!",
						    "<FONT SIZE=\"+2\">"
						    "The martians have "
						    "invaded and have taken "
						    "over <B>all</B> the "
						    "governments "
						    "of the world.  Life as "
						    "you know it has just "
						    "ended</FONT>",
						    10,
						    &uniqueID);
		break;
	case 2:
		grapevine_notification_post_simple_notice(client, NULL,
						    GRAPEVINE_MEDIUM_RARE,
						    keywords,
						    "Flamewar on Slashdot!",
						    "Wow, what news, "
						    "there was a <B>"
						    "flamewar</B> on "
						    "<A HREF=\"http:"
						    "//slashdot.org/\">"
						    "slashdot</A>!",
						    10,
						    &uniqueID);
		break;
	case 3:
		grapevine_notification_post_simple_notice(client, NULL,
						    GRAPEVINE_MEDIUM,
						    keywords,
						    "CPU Melted!",
						    "Your CPU has melted! "
						    "You might have to buy "
						    "a new one.",
						    10,
						    &uniqueID);
		break;
	default:
		puts("URK???");
	}


	g_print("Added simple UniqueID: %s\n", uniqueID);
	g_free(uniqueID);

	return TRUE;
}

static gboolean
remove_notice(gpointer data)
{
	GrapevineNotice *nc = data;

	grapevine_notice_remove(nc);

	gtk_object_unref(GTK_OBJECT(nc));

	return FALSE;
}

static void
notice_destroyed(GtkObject *notice)
{
	progress_notices = g_list_remove(progress_notices, notice);
}

static gboolean
add_to_remove(gpointer data)
{
	GrapevineNotification *client = data;
	GrapevineNotice *nc;
	char *uniqueID = NULL;
	GList *li;
	int progress;
	char *s, *not;

	nc = grapevine_notification_create_notice(client, NULL);
	if( ! nc) {
		g_warning(_("Cannot create notice"));
		return TRUE;
	}
	progress = (rand()%101) - 1;
	grapevine_notice_set_priority(nc, GRAPEVINE_INFORMATIONAL);
	grapevine_notice_set_summary(nc, "Summary");


	if (app != NULL) {
		s = grapevine_make_goto_window_url (GTK_WINDOW (app));
		not = g_strdup_printf ("<B>Content</B><P>"
				       "<A HREF=\"%s\">"
				       "<IMG SRC=\"test.png\"></A>", s);
	} else {
		s = NULL;
		not = g_strdup ("GEGLGEGLEGL"
				"<IMG SRC=\"test.png\">");
	}

	grapevine_notice_set_content(nc, not);

	puts ("NOTICE:");
	puts (not);

	g_free (s);
	g_free (not);

	grapevine_notice_add_data_from_file(nc, "test.png", "test.png");
	grapevine_notice_set_post_sound_from_file(nc, "test.wav");
	grapevine_notice_set_view_sound_from_file(nc, "test.wav");
	grapevine_notice_set_progress(nc, progress);

	uniqueID = grapevine_notice_get_uniqueID(nc);

	g_print("Added complex UniqueID: %s\n", uniqueID);
	g_free(uniqueID);

	for(li = progress_notices; li != NULL; li = li->next)
		grapevine_notice_add_relation(nc, li->data);
	progress_notices = g_list_prepend(progress_notices, nc);

	gtk_signal_connect(GTK_OBJECT(nc), "destroy",
			   GTK_SIGNAL_FUNC(notice_destroyed),
			   NULL);

	grapevine_notice_post(nc);

	gtk_timeout_add(25000, remove_notice, nc);

	return TRUE;
}

int
main (int argc, char *argv[])
{
	GtkWidget *w;
	GrapevineNotification *client;

	bindtextdomain (PACKAGE, GNOMELOCALEDIR);
	textdomain (PACKAGE);
	
        gnome_init_with_popt_table ("grapevine-test", VERSION, 
				    argc, argv, oaf_popt_options, 0, NULL);

	if( ! grapevine_init(argc, argv, TRUE)) {
		GtkWidget *w =
			gnome_error_dialog(_("Cannot initialize grapevine"));
		gnome_dialog_run(GNOME_DIALOG(w));
		return 1;
	}

	bonobo_activate();

	client = grapevine_notification_new();
	grapevine_notification_set_default_source(client,
						  "Grapevine/TestClient");


	grapevine_notification_post_simple_notice(client,
						  NULL /*use default source*/,
						  GRAPEVINE_INFORMATIONAL,
						  NULL,
						  "GEGL!",
						  "GEGL CONTENT!",
						  70,
						  NULL);


	gtk_timeout_add(8000, add_simple, client);
	gtk_timeout_add(5000, add_to_remove, client);

	add_to_remove(client);

	app = gnome_app_new("grapevine-test", "Grapevine Test Client");
	gtk_signal_connect(GTK_OBJECT(app), "destroy",
			   GTK_SIGNAL_FUNC(gtk_main_quit),
			   NULL);
	w = gtk_button_new_with_label("QUIT!");
	gtk_signal_connect(GTK_OBJECT(w), "clicked",
			   GTK_SIGNAL_FUNC(gtk_main_quit),
			   NULL);
	gnome_app_set_contents(GNOME_APP(app), w);
	gtk_widget_show_all(app);

	bonobo_main();

	gtk_object_unref(GTK_OBJECT(client));

	return 0;
}
