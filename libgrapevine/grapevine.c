/* Grapevine: GNOME Notifications
 *   Main librapevine file
 * Author: George Lebl
 * (c) 2000 Eazel, Inc.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.	 See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#include "config.h"
#include <gnome.h>
#include <bonobo.h>
#include <liboaf/liboaf.h>
#include <gdk/gdkx.h>

#include "grapevine-i18n.h"

#include "grapevine.h"

/*
 * Some globals
 */
CORBA_ORB grapevine_orb = CORBA_OBJECT_NIL;

/*
 * Some local globals
 */
static gboolean grapevine_initialized = FALSE;

/**
 * grapevine_init:
 * @argc:
 * @argv:
 * @initialize_bonobo:
 *
 * Description:  Initialize grapevine
 *
 * Returns:  %TRUE if everything went ok, %FALSE if there is an error.
 **/
gboolean
grapevine_init (int argc, char **argv, gboolean initialize_bonobo)
{
	/* already initialized so just ignore this */
	if(grapevine_initialized)
		return TRUE;

	if( ! oaf_is_initialized()) {
		grapevine_orb = oaf_init(argc, argv);
	} else {
		grapevine_orb = oaf_orb_get();
	}

	if(grapevine_orb == CORBA_OBJECT_NIL) {
		g_warning(_("Cannot initialize OAF for Grapevine"));
		return FALSE;
	}

	if(initialize_bonobo)
		bonobo_init (grapevine_orb, CORBA_OBJECT_NIL, CORBA_OBJECT_NIL);

	return TRUE;
}

gboolean
grapevine_is_initialized(void)
{
	return grapevine_initialized;
}

char *
grapevine_make_goto_window_url (GtkWindow *win)
{
	GdkWindow *gdkwin;

	g_return_val_if_fail (win != NULL, NULL);
	g_return_val_if_fail (GTK_IS_WINDOW (win), NULL);
	g_return_val_if_fail (GTK_WIDGET_REALIZED (win), NULL);

	gdkwin = GTK_WIDGET (win)->window;
	if (gdkwin == NULL)
		return NULL;

	return g_strdup_printf ("x-goto-window:%lu",
				(gulong) (GDK_WINDOW_XWINDOW (gdkwin)));
}
