/* Grapevine: GNOME Notifications
 *   Main librapevine file
 * Author: George Lebl
 * (c) 2000 Eazel, Inc.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.	 See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#ifndef GRAPEVINE_H
#define GRAPEVINE_H

/*
 * Required includes
 */
#include <gnome.h>
#include <bonobo.h>

/*
 * Client objects includes
 */
#include "grapevine-notice-listener-impl.h"
#include "grapevine-notice.h"
#include "grapevine-notification.h"

BEGIN_GNOME_DECLS

/*
 * main library stuff
 */
gboolean	grapevine_init			(int argc,
						 char **argv,
						 gboolean initialize_bonobo);
gboolean	grapevine_is_initialized	(void);


/*
 * Some utilities
 */
char *		grapevine_make_goto_window_url	(GtkWindow *win);

END_GNOME_DECLS

#endif /* GRAPEVINE_H */
