#!/bin/sh
# Run this to generate all the initial makefiles, etc.

srcdir=`dirname $0`
test -z "$srcdir" && srcdir=.

PKG_NAME="Grapevine"

(test -f $srcdir/configure.in \
  && test -f $srcdir/src/grapevine-main.gob) || {
    echo -n "**Error**: Directory "\`$srcdir\'" does not look like the"
    echo " top-level gnome properties directory"
    exit 1
}

. $srcdir/macros/autogen.sh
