#include <Bonobo.idl>

module GNOME {
	module Grapevine {
		interface Notice;

		enum NoticePriority {
			CRITICAL,
			MEDIUM,
			MEDIUM_RARE,
			INFORMATIONAL
		};

		enum NoticeState {
			NOTICE_PENDING,
			NOTICE_POSTED,
			NOTICE_DEAD
		};

		typedef sequence<string> StringSeq;
		typedef sequence<Notice> NoticeSeq;

		typedef sequence<octet> DataSeq;

		interface NoticeListener : Bonobo::Unknown {
			/* these are oneway so that a hanging client
			 * does not hang the server */
			oneway void postedNotice (in Notice notice);
			oneway void changedNotice (in Notice notice);
			oneway void removedNotice (in Notice notice);
		};

		/* Objects which implement this interface are objects
		   that want to be notified when a new notice is posted.
		   such as a pager interface */
		interface NoticeHandler : Bonobo::Unknown {
			/* Only get a notice id, not Notice object,
			 * this is because there is no Notification
			 * object yet. */
			oneway void postedNotice (in string noticeID);
		};

		interface Notification : Bonobo::Unknown {
			/* create a new notice */
			Notice createNotice (in string source);

			/* find an existing notice */
			Notice findNotice (in string uniqueID);

			/* gets all the current notices, an outside view should
			 * do this and then set a listener */
			readonly attribute NoticeSeq currentNotices;

			/* This will give information about ALL notices, not
			 * just specific ones.  There is only one listener per
			 * Notification, because each client has it's own
			 * Notification object.  To unset a listener, just pass
			 * CORBA_OBJECT_NIL */
			void setListener (in NoticeListener listener);

			/* put up the browse dialog */
			void runBrowseDialog ();

			/* put up the properties dialog */
			void runPropertiesDialog ();

			/* A set of languages to use.  The first one that fits
			 * should be used */
			readonly attribute StringSeq languages;

			/* probably some other attributes/methods related to
			 * GUI */
		};

		interface Notice : Bonobo::Unknown {
			
			/* this will trigger the notice to be removed.  If the
			 * caller has installed a listener, the caller should
			 * not unref the notice until the listener gets the
			 * notice_removed call */
			oneway void remove ();

			/* when the application that created the notice has
			 * setup all it's attributes, it should call this as a
			 * way of saying this notice is ready for display.
			 * Until this is called the notice is not stored and
			 * doesn't appear in any list */
			oneway void post ();

			/* Add or remove a listener.  There can be any number of
			 * listeners on a notice */
			void addListener (in NoticeListener listener);
			void removeListener (in NoticeListener listener);

			readonly attribute NoticeState state;

			attribute NoticePriority priority;

			/* browsing categories, keywords */
			attribute StringSeq keywords;

			/* plain text for now, perhaps HTML */
			attribute string summary;
			/*HTML with special tags, for now
			  let's assume gtkhtml lets us do
			  that*/
			attribute string content; 

			/* the source program's name */
			readonly attribute string source;

			/* a unique id assigned */
			readonly attribute string uniqueID;

			/* get and set the expiration time, in seconds,
			 * the _at is in time_t format, the relative is
			 * from 'now', why is it signed? because it might
			 * be in the past for some reason. */
			attribute long expireIn; /* relative */
			attribute unsigned long expireAt; /* absolute */

			/* the related notices */
			readonly attribute NoticeSeq related;
			oneway void addRelation (in Notice notice);
			oneway void removeRelation (in Notice notice);

			/* if negative, then no progress bar.  If greater then
 			 * or equal to 0, then grapevine will display a
			 * progress bar */
			attribute short progress;

			/* send some raw data typed by a file, this is used for
 			 * images and sounds in the notice */
			oneway void addData (in string file, in DataSeq data);
			oneway void removeData (in string file);
			DataSeq getData (in string file);

			/* Sounds, these refer to some raw data as set above */
			/* Sound to play when posted */
			attribute string postSound;
			/* Sound to play when viewed
			   (The view dialog is opened) */
			attribute string viewSound;
		};
	};
};
