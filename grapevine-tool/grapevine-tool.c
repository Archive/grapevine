/* Grapevine: GNOME Notifications
 *   Test client
 * Author: George Lebl
 * (c) 2000 Eazel, Inc.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.	 See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#include "config.h"
#include <gnome.h>
#include <bonobo.h>
#include <liboaf/liboaf.h>

#include "grapevine.h"

static gboolean informational = FALSE;
static gboolean medium = FALSE;
static gboolean medium_rare = FALSE;
static gboolean critical = FALSE;
static char *source = NULL;
static char *keywords = NULL;
static char *summary = NULL;
static char *contents = NULL;
static char *file = NULL;
static int expire_in = 0;


static struct poptOption options[] = {
	{ "informational", 'i', POPT_ARG_NONE, &informational, 0, N_("Informational priority notice"), NULL },
	{ "medium-rare", '\0', POPT_ARG_NONE, &medium_rare, 0, N_("Medium-Rare priority notice"), NULL },
	{ "medium", 'm', POPT_ARG_NONE, &medium, 0, N_("Medium priority notice"), NULL },
	{ "critical", 'r', POPT_ARG_NONE, &critical, 0, N_("Critical priority notice"), NULL },
	{ "source", 'z', POPT_ARG_STRING, &source, 0, N_("Source string (e.g. Grapevine/Tool)"), N_("SOURCE") },
	{ "keywords", 'k', POPT_ARG_STRING, &keywords, 0, N_("Comma separated list of keywords"), N_("KEYWORDS") },
	{ "summary", 's', POPT_ARG_STRING, &summary, 0, N_("Summary string"), N_("SUMMARY") },
	{ "contents", 'c', POPT_ARG_STRING, &contents, 0, N_("Contents string"), N_("CONTENTS") },
	{ "file", 'f', POPT_ARG_STRING, &file, 0, N_("File to read contents from (- for stdin)"), N_("FILE") },
	{ "expire_in", 'e', POPT_ARG_INT, &expire_in, 0, N_("Number of seconds to expire in (0 for no expire)"), N_("SECONDS") },

	{ NULL, '\0', POPT_ARG_INCLUDE_TABLE, &oaf_popt_options, 0, NULL, NULL },
	POPT_AUTOHELP
	{ NULL, '\0', 0, NULL, 0, NULL, NULL }
};


static void
post_notice (int priority,
	     const char *source,
	     const char *keywords,
	     const char *summary,
	     const char *contents,
	     int expire_in)
{
	GrapevineNotification *client;
	char **keywordsv;

	if (keywords != NULL)
		keywordsv = g_strsplit (keywords, ",", -1);
	else
		keywordsv = NULL;

	client = grapevine_notification_new ();
	grapevine_notification_set_default_source (client,
						  "Grapevine/Tool");

	grapevine_notification_post_simple_notice (client,
						   source,
						   priority,
						   keywordsv,
						   summary,
						   contents,
						   expire_in,
						   NULL);
	
	gtk_object_unref(GTK_OBJECT(client));
}

static void
ensure_summary_contents (void)
{
	if (summary == NULL)
		summary = g_strdup ("Message from grapevine-tool");

	/* if no contents specified, not even a file,
	 * assume file was "-" and read from stdin */
	if (file == NULL &&
	    contents == NULL)
		file = "-";

	if (file != NULL) {
		char buf[256];
		FILE *fp = NULL;
		GString *str;

		if (strcmp (file, "-") == 0) {
			fp = stdin;
		} else {
			fp = fopen (file, "r");
			if (fp == NULL) {
				fprintf (stderr,
					 _("Could not open %s for reading"),
					 file);
				exit (1);
			}
		}

		/* append onto specified contents */
		str = g_string_new (contents);

		while (fgets (buf, sizeof (buf), fp)) {
			g_string_append (str, buf);
		}

		if (fp != stdin)
			fclose (fp);

		g_free (contents);
		contents = str->str;
		g_string_free (str, FALSE);
	}
}


int
main (int argc, char *argv[])
{
	int priority;

	bindtextdomain (PACKAGE, GNOMELOCALEDIR);
	textdomain (PACKAGE);
	
        gnome_init_with_popt_table ("grapevine-test", VERSION, 
				    argc, argv, options, 0, NULL);

	ensure_summary_contents ();

	if( ! grapevine_init(argc, argv, TRUE)) {
		GtkWidget *w =
			gnome_error_dialog(_("Cannot initialize grapevine"));
		gnome_dialog_run(GNOME_DIALOG(w));
		return 1;
	}

	bonobo_activate();

	if (critical)
		priority = GRAPEVINE_CRITICAL;
	else if (medium)
		priority = GRAPEVINE_MEDIUM;
	else if (medium_rare)
		priority = GRAPEVINE_MEDIUM_RARE;
	else
		priority = GRAPEVINE_INFORMATIONAL;

	post_notice (priority, source,
		     keywords, summary, contents, 
		     expire_in);

	return 0;
}
