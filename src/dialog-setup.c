/* Grapevine: GNOME Notifications
 *   Generic dialog setup
 * Author: George Lebl
 * (c) 2000 Eazel, Inc.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#include "config.h"
#include <gnome.h>
#include <stdlib.h>

#include <glade/glade.h>
#include "glade-helper.h"
#include "grapevine-util.h"

#include "dialog-setup.h"

/* some evilness follows */
static void
dlg_destroyed (GtkWidget *dialog)
{
	int handler;
	GSList *cl = gtk_object_get_data(GTK_OBJECT(dialog), "coordspeed");
	GSList *wl = gtk_object_get_data(GTK_OBJECT(dialog), "ballwin");
	grp_slist_free_deep (cl);
	g_slist_free(wl);
	handler = GPOINTER_TO_INT(gtk_object_get_data(GTK_OBJECT(dialog),
						      "handler"));
	gtk_timeout_remove(handler);
	gtk_object_set_data(GTK_OBJECT(dialog), "coordspeed", NULL);
	gtk_object_set_data(GTK_OBJECT(dialog), "ballwin", NULL);
	gtk_object_set_data(GTK_OBJECT(dialog), "handler", NULL);
}

/* I AAAAAM YOUUUUUUR FAAAAAAATTTTTHHHHHHHEEEEERRRRRR */
static gboolean
ball_move(gpointer data)
{
	GtkWidget *dialog = data;
	GSList *cl = gtk_object_get_data(GTK_OBJECT(dialog), "coordspeed");
	GSList *wl = gtk_object_get_data(GTK_OBJECT(dialog), "ballwin");

	while(cl && wl) {
		int *coordspeed = cl->data;
		GdkWindow *win = wl->data;

		coordspeed[0] += coordspeed[2];
		coordspeed[1] += coordspeed[3];
		if(coordspeed[0] <= 0)
			coordspeed[2] = ABS(coordspeed[2]);
		if(coordspeed[0] >= dialog->allocation.width - 20)
			coordspeed[2] = - ABS(coordspeed[2]);
		if(coordspeed[1] <= 0)
			coordspeed[3] = ABS(coordspeed[3]);
		if(coordspeed[1] >= dialog->allocation.height - 20)
			coordspeed[3] = - ABS(coordspeed[3]);

		gdk_window_move(win, coordspeed[0], coordspeed[1]);

		cl = cl->next;
		wl = wl->next;
	}

	return TRUE;
}

/* the incredibly evil function */
static void
doblah(GtkWidget *dialog)
{
	GdkWindowAttr attributes;
	int *coordspeed;
	GdkPixmap *pixmap;
	GdkBitmap *bitmap;
	GdkWindow *win;
	int handler;
	GSList *cl, *wl;
	char * ball_xpm[] = {
		"20 20 5 1",
		".	c #6398E8",
		"+	c #0964ED",
		"#	c #0F54BC",
		" 	c None",
		"@	c #FFFFFF",
		"       .....        ", "    ....+++....     ",
		"   ....@@@+++++#    ", "  ...@@@+++++++##   ",
		"  ..@@++++++++++#   ", " ..@@+++++++++++##  ",
		" ..@++++++++++++##  ", "..@@+++++++++++++## ",
		"..@+++++++++++++### ", ".+@+++++++++++++### ",
		".+++++++++++++++### ", ".++++++++++++++#### ",
		"..+++++++++++++#### ", " .++++++++++++####  ",
		" .+++++++++++#####  ", "  ++++++++++#####   ",
		"  ##++++++#######   ", "   ###+#########    ",
		"    ###########     ", "       #####        "
	};

	ball_xpm[1] = g_strdup(ball_xpm[1]);
	ball_xpm[2] = g_strdup(ball_xpm[2]);
	ball_xpm[3] = g_strdup(ball_xpm[3]);

#define FOOSWAP(i,a,b) { char tmp; char *foo = ball_xpm[i]; tmp = foo[a]; \
	foo[a] = foo[b]; foo[b] = tmp; tmp = foo[a+1]; foo[a+1] = foo[b+1]; \
	foo[b+1] = tmp; }
#define BARSWAP(a,b) FOOSWAP(1,a,b); FOOSWAP(2,a,b); FOOSWAP(3,a,b);

	switch(rand() % 6) {
	case 0: break;
	case 1: BARSWAP(5,7); break;
	case 2: BARSWAP(7,9); break;
	case 3: BARSWAP(5,9); break;
	case 4: BARSWAP(5,7); BARSWAP(7,9); break;
	case 5: BARSWAP(5,9); BARSWAP(7,9); break;
	default: break;
	}

#undef FOOSWAP
#undef BARSWAP
	pixmap = gdk_pixmap_create_from_xpm_d(dialog->window, &bitmap,
					      NULL, (char **)ball_xpm);
	if( ! pixmap)
		return;

	g_free(ball_xpm[1]);
	g_free(ball_xpm[2]);
	g_free(ball_xpm[3]);


	coordspeed = g_new(gint, 4);
	cl = gtk_object_get_data(GTK_OBJECT(dialog), "coordspeed");
	cl = g_slist_prepend(cl, coordspeed);
	gtk_object_set_data(GTK_OBJECT(dialog), "coordspeed", cl);
	coordspeed[0] = (rand() % (dialog->allocation.width - 20 - 2)) + 1;
	coordspeed[1] = (rand() % (dialog->allocation.height - 20 - 2)) + 1;
	coordspeed[2] = (rand() % 12) + 2;
	coordspeed[3] = (rand() % 12) + 2;

	attributes.window_type = GDK_WINDOW_CHILD;
	attributes.x = coordspeed[0];
	attributes.y = coordspeed[1];
	attributes.width = 20;
	attributes.height = 20;
	attributes.wclass = GDK_INPUT_OUTPUT;
	attributes.visual = gtk_widget_get_visual(dialog);
	attributes.colormap = gtk_widget_get_colormap(dialog);
	attributes.event_mask = 0;

	win = gdk_window_new(dialog->window, &attributes,
			     GDK_WA_X | GDK_WA_Y |
			     GDK_WA_VISUAL | GDK_WA_COLORMAP);
	gdk_window_set_back_pixmap(win, pixmap, FALSE);
	gdk_window_shape_combine_mask(win, bitmap, 0, 0);

	gdk_window_show(win);

	gdk_pixmap_unref(pixmap);
	gdk_bitmap_unref(bitmap);

	wl = gtk_object_get_data(GTK_OBJECT(dialog), "ballwin");
	wl = g_slist_prepend(wl, win);
	gtk_object_set_data(GTK_OBJECT(dialog), "ballwin", wl);

	handler = GPOINTER_TO_INT(gtk_object_get_data(GTK_OBJECT(dialog),
						      "handler"));
	if( ! handler) {
		handler = gtk_timeout_add(100, ball_move, dialog);
		gtk_object_set_data(GTK_OBJECT(dialog), "handler",
				    GINT_TO_POINTER(handler));
		gtk_signal_connect(GTK_OBJECT(dialog), "destroy",
				   GTK_SIGNAL_FUNC(dlg_destroyed), NULL);
	}
}

/* only slightly evil function */
static gboolean
foo_keypress(GtkWidget *widget, GdkEventKey *event)
{
	static int foo = 0;

	if( ! (event->state & GDK_CONTROL_MASK) ||
	    foo >= 5)
		return FALSE;

	switch (event->keyval) {
	case GDK_e:
	case GDK_E:
		if(foo == 4) { doblah(widget); } foo = 0; break;
	case GDK_m:
	case GDK_M:
		if(foo == 3) { foo++; } else { foo = 0; } break;
	case GDK_o:
	case GDK_O:
		if(foo == 2) { foo++; } else { foo = 0; } break;
	case GDK_n:
	case GDK_N:
		if(foo == 1) { foo++; } else { foo = 0; } break;
	case GDK_g:
	case GDK_G:
		if(foo == 0) { foo++; } else { foo = 0; } break;
	default:
		foo = 0;
	}

	return FALSE;
}

void
dialog_setup (GtkWidget *dialog, GtkWindow *parent, GtkObject *alive_object)
{
	g_return_if_fail (dialog != NULL);
	g_return_if_fail (GNOME_IS_DIALOG(dialog));

	gtk_signal_connect (GTK_OBJECT (dialog), "key_press_event",
			    GTK_SIGNAL_FUNC (foo_keypress), NULL);

	if (parent != NULL)
		gnome_dialog_set_parent (GNOME_DIALOG (dialog), parent);

	if (alive_object != NULL)
		gtk_signal_connect_object_while_alive
			(GTK_OBJECT (alive_object), "destroy",
			 GTK_SIGNAL_FUNC (gnome_dialog_close),
			 GTK_OBJECT (dialog));
}

GtkWidget *
dialog_do_dialog (GladeXML *xml, const char *name,
		  GtkWindow *parent, GtkObject *alive_object)
{
	GtkWidget *dialog;

	g_return_val_if_fail (xml != NULL, NULL);
	g_return_val_if_fail (name != NULL, NULL);

	dialog = glade_helper_get (xml, name, gnome_dialog_get_type ());

	dialog_setup (dialog, parent, alive_object);

	return dialog;
}
