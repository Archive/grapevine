/* Grapevine: GNOME Notifications
 *   Main file
 * Author: George Lebl
 * (c) 2000 Eazel, Inc.
 * (c) 2001 George Lebl
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#include "config.h"
#include <gnome.h>

#include <glade/glade.h>
#include <liboaf/liboaf.h>
#include <bonobo.h>
#include <libgnorba/gnorba.h>
#include <gconf/gconf.h>
#include <gconf/gconf-client.h>
#include <pong/pong.h>
#include <pong/pong-glade.h>
#include <pong-bonobo/pong-bonobo.h>
#include <pong-bonobo/pong-bonobo-entry.h>
#include <gtkhtml/gtkhtml.h>

#include "gnome-grapevine.h"
#include "grapevine-main.h"
#include "grapevine-notification-impl.h"
#include "grapevine-notice-impl.h"

#include "grapevine-keywords-list.h"

#include "grapevine-util.h"
#include "browse-dialog.h"
#include "applet.h"

#include "defines.h"

#include "grapevine-oafiid.h"

/*
 * Global variables
 */
GrapevineMain *main_object = NULL;
int active_notification_servers = 0;
int active_views = 0;
int active_pong_widgets = 0;
GConfClient *client = NULL;
PongXML *config = NULL;

/*
 * Local globals
 */
static BonoboGenericFactory *factory = NULL;
static int notice_save_queue_handler_id = 0;
static BonoboGenericFactory *pong_factory = NULL;

/*
 * Some definitions
 */
/* the timeout for saving to file once something has changed */
#define NOTICE_SAVE_TIMEOUT 10000 /* 10 seconds */

static void
notification_destroy (GrapevineNotificationImpl *not, gpointer data)
{
	active_notification_servers --;
	gtk_main_quit ();
}

static void
run_browse_dialog (GrapevineNotificationImpl *not, gpointer data)
{
	browse_dialog_show ();
}

static void
run_properties_dialog (GrapevineNotificationImpl *not, gpointer data)
{
	if (config != NULL) {
		pong_xml_show_dialog (config);
	} else {
		config = grapevine_properties_show ("gnome-grapevine.pong",
						    NULL);
		/* Most likely the dialog has already taken ref of the
		 * PongXML object, and we want to keep it around as long
		 * as the dialog is around */
		gtk_signal_connect (GTK_OBJECT (config), "destroy",
				    GTK_SIGNAL_FUNC (gtk_widget_destroyed),
				    &config);

		/* loose our ref, we only keep a weak reference */
		gtk_object_unref (GTK_OBJECT (config));
	}
}


static BonoboObject *
make_notification (BonoboGenericFactory *factory, 
		   gpointer closure)
{
	BonoboObject *not;

	puts ("CREATING NEW NOTIFICATION");

	not = grapevine_notification_impl_new ();

	active_notification_servers ++;
	puts ("CREATING NEW NOTIFICATION2");

	gtk_signal_connect (GTK_OBJECT (not), "destroy",
			    GTK_SIGNAL_FUNC (notification_destroy),
			    NULL);

	gtk_signal_connect (GTK_OBJECT (not), "run_browse_dialog",
			    GTK_SIGNAL_FUNC (run_browse_dialog),
			    NULL);
	gtk_signal_connect (GTK_OBJECT (not), "run_properties_dialog",
			    GTK_SIGNAL_FUNC (run_properties_dialog),
			    NULL);

	return not;
}

static void
pong_control_destroyed (BonoboObject *control, gpointer data)
{
	active_pong_widgets --;
	gtk_main_quit ();
}

static BonoboObject *
make_pong_widget (BonoboGenericFactory *factory,
		  const char *component_id,
		  gpointer closure)
{
	if (strcmp (component_id, GRAPEVINE_KEYWORDS_LIST_OAFIID) == 0) {
		GtkWidget *keywords_list = grapevine_keywords_list_new ();
		BonoboControl *control;

		control = pong_bonobo_entry_make_control (keywords_list);

		if (control != NULL) {
			active_pong_widgets ++;
			gtk_signal_connect (GTK_OBJECT (control), "destroy",
					    GTK_SIGNAL_FUNC (pong_control_destroyed),
					    NULL);
		} else {
			gtk_widget_destroy (keywords_list);
		}

		/* could be NULL, so don't use the checking macro */
		return (BonoboObject *) control;
	} else {
		g_warning (_("Requested invalid PonG control '%s'"),
			   component_id);
		return NULL;
	}
}

static void
read_notices_from_disk (void)
{
	char *notice_file, *data_dir;

	notice_file = gnome_util_home_file ("GrapevineNotices");
	data_dir = gnome_util_home_file ("GrapevineNotices.d");

	if (g_file_exists (notice_file)) {
		grapevine_main_read_from_file (main_object, notice_file,
					       data_dir);
	}

	g_free (notice_file);
	g_free (data_dir);
}

static void
save_notices_to_disk (void)
{
	char *notice_file, *data_dir;

	notice_file = gnome_util_home_file ("GrapevineNotices");
	data_dir = gnome_util_home_file ("GrapevineNotices.d");

	grapevine_main_write_to_file (main_object, notice_file, data_dir);

	g_free (notice_file);
	g_free (data_dir);
}

static gboolean
notice_save_queue_handler (gpointer data)
{
	notice_save_queue_handler_id = 0;

	save_notices_to_disk ();

	return FALSE;
}

static void
queue_notice_save (void)
{
	if (notice_save_queue_handler_id == 0)
		notice_save_queue_handler_id = 
			gtk_timeout_add (NOTICE_SAVE_TIMEOUT,
					 notice_save_queue_handler,
					 NULL);
}

/* UTTER HACK */
static ORBit_MessageValidationResult
accept_all_cookies (CORBA_unsigned_long request_id,
		    CORBA_Principal *principal,
		    CORBA_char *operation)
{
	/* allow ALL cookies */
	return ORBIT_MESSAGE_ALLOW_ALL;
}


int
main (int argc, char *argv[])
{
	CORBA_ORB orb;
	CORBA_ORB goad_orb;
	CORBA_Environment ev;
	GError* error = NULL;

	bindtextdomain (PACKAGE, GNOMELOCALEDIR);
	textdomain (PACKAGE);
	
	CORBA_exception_init (&ev);

	gnome_client_disable_master_connection ();

	/* Init GNOME and gnorba */
	goad_orb = gnome_CORBA_init_with_popt_table ("gnome-grapevine",
						     VERSION,
						     &argc, argv,
						     oaf_popt_options, 0, NULL,
						     GNORBA_INIT_SERVER_FUNC,
						     &ev);

	gdk_rgb_init ();

	/* want to accept all corba messages so we setup the request validator
	 * to just "accept all".  With Orbit 5.1 and higher this should be
	 * secure */
	ORBit_set_request_validation_handler (accept_all_cookies);

	/* Initialize components and libraries we use */
	glade_gnome_init ();
	orb = oaf_init (argc, argv);
	bonobo_init (orb, CORBA_OBJECT_NIL, CORBA_OBJECT_NIL);

	/* Set up GCONF */
	if ( ! gconf_init (argc, argv, &error)) {
		g_assert (error != NULL);
		g_warning (_("GConf init failed:\n  %s"), error->message);
		g_error_free (error);
		error = NULL;
		return 1;
	}
	client = gconf_client_get_default ();

	gconf_client_add_dir (client, "/apps/grapevine",
			      GCONF_CLIENT_PRELOAD_NONE, NULL);
	gconf_client_add_dir (client, "/apps/grapevine-applet",
			      GCONF_CLIENT_PRELOAD_NONE, NULL);

	/* Set up PONG */
	if ( ! pong_init ()) {
		g_warning (_("PonG init failed:"));
		return 1;
	}
	pong_bonobo_init ();
	pong_add_directory (PONGFILEDIR);

	/*XXX: for debugging */
	pong_add_glade_directory ("../glade/");

	pong_add_glade_directory (GLADEFILEDIR);

	/* Create the main grapevine storage */
	main_object = grapevine_main_new ();
	gtk_signal_connect_after (GTK_OBJECT (main_object), "post_notice",
				  GTK_SIGNAL_FUNC (queue_notice_save),
				  NULL);
	gtk_signal_connect_after (GTK_OBJECT (main_object), "remove_notice",
				  GTK_SIGNAL_FUNC (queue_notice_save),
				  NULL);
	gtk_signal_connect_after (GTK_OBJECT (main_object), "change_notice",
				  GTK_SIGNAL_FUNC (queue_notice_save),
				  NULL);

	read_notices_from_disk ();
	
	/* Create the factory for Notification object */
	factory = bonobo_generic_factory_new
		(GRAPEVINE_NOTIFICATION_FACTORY_OAFIID,
		 make_notification, NULL);

	/* Create the factory for PonG widgets */
	pong_factory = bonobo_generic_factory_new_multi
		(GRAPEVINE_PONG_WIDGETS_FACTORY_OAFIID,
		 make_pong_widget,
		 NULL);

	/* add the applet factory */
	applet_add_factory ();

	puts ("GRAPEVINE STARTED");

	do
		bonobo_main ();
	while (active_views > 0 ||
	       active_notification_servers > 0 ||
	       active_pong_widgets > 0);

	bonobo_object_unref (BONOBO_OBJECT (factory));
	bonobo_object_unref (BONOBO_OBJECT (pong_factory));

	/* save current notices to disk */
	save_notices_to_disk ();

	gtk_object_unref (GTK_OBJECT (main_object));

	gconf_client_remove_dir (client, "/apps/grapevine", NULL);
	gconf_client_remove_dir (client, "/apps/grapevine-applet", NULL);

	gtk_object_unref (GTK_OBJECT (client));

	CORBA_exception_free (&ev);
	
	return 0;
}
