/* Grapevine: GNOME Notifications
 *   Browse dialog
 * Author: George Lebl
 * (c) 2000 Eazel, Inc.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#include "config.h"
#include <gnome.h>

#include <glade/glade.h>

#include "grapevine-main.h"
#include "grapevine-notice-real.h"

#include "grapevine-util.h"
#include "glade-helper.h"
#include "dialog-setup.h"
#include "view.h"

#include "browse-dialog.h"

/*
 * Externs
 */
extern int active_views;
extern GrapevineMain *main_object;

/*
 * Local globals
 */
static GtkWidget *browse_dialog = NULL;
static GtkWidget *browse_dialog_clist = NULL;
static GtkWidget *browse_dialog_arrows[4] = {0};
static GtkWidget *browse_keywords_entry = NULL;
static char **browse_keywords = NULL;
static GtkWidget *keyword_select_dialog = NULL;

/*
 * Some constants
 */
#define KEYWORD_SEPARATORS " \t\n\r,;/+"

static void
browse_dialog_destroy(GtkWidget *dialog, gpointer data)
{
	puts("BROWSE_DIALOG_DESTROY");
	if(keyword_select_dialog) {
		gtk_widget_destroy(keyword_select_dialog);
	}
	gtk_signal_disconnect_by_data(GTK_OBJECT(main_object), browse_dialog);
	browse_dialog = NULL;
	active_views --;
	gtk_main_quit();
}

static char **
fill_notice_info_row(GrapevineNoticeReal *notice)
{
	char **t;
	char tmp[256];
	time_t tstamp;
	/* Note, this has to be in sync with the IDL */
	/* Note, it should really be icons */
	char *priorities[] = {
		N_("CRITICAL"),
		N_("MEDIUM"),
		N_("MEDIUM_RARE"),
		N_("INFORMATIONAL")
	};

	t = g_new0(char *, 5);

	tstamp = notice->timestamp;
	if (strftime (tmp, sizeof(tmp), _("%l:%M:%S %p"),
		      localtime (&tstamp)) == 0) {
		/* according to docs, if the string does not fit, the
		 * contents of tmp2 are undefined, thus just use
		 * ??? */
		strcpy (tmp, "???");
	}
	tmp[sizeof (tmp) - 1] = '\0'; /* just for sanity */

	t[0] = g_strdup (tmp);
	t[1] = g_strdup (_(priorities[notice->priority]));
	if (notice->progress >= 0)
		t[2] = g_strdup_printf (_("%s (%d%%)"), notice->summary,
					notice->progress);
	else
		t[2] = g_strdup (notice->summary);
	t[3] = notice->keywords ?
		g_strjoinv(", ", notice->keywords) :
		g_strdup(_("None"));
	t[4] = NULL;

	return t;
}

static void
browse_dialog_clist_append_notice(GrapevineNoticeReal *notice)
{
	int row;
	char **t;

	/*
	 * First we check if this notice is allowed by the keywords
	 */
	if (browse_keywords) {
		int i, j;
		gboolean found = FALSE;

		/* this cannot possibly match then */
		if(notice->keywords == NULL)
			return;

		for(i = 0; browse_keywords[i] != NULL && !found; i++) {
			for(j = 0; notice->keywords[j] != NULL && !found; j++) {
				if(g_strcasecmp(browse_keywords[i],
						notice->keywords[j]) == 0)
					found = TRUE;
			}
		}
		if( ! found)
			return;
	}

	t = fill_notice_info_row(notice);

	row = gtk_clist_append(GTK_CLIST(browse_dialog_clist), t);

	g_strfreev(t);

	gtk_object_ref(GTK_OBJECT(notice));
	gtk_clist_set_row_data_full(GTK_CLIST(browse_dialog_clist),
				    row, notice,
				    (GtkDestroyNotify)gtk_object_unref);

	gtk_clist_sort(GTK_CLIST(browse_dialog_clist));
}

static void
browse_dialog_clist_fill(void)
{
	GList *li;

	gtk_clist_clear(GTK_CLIST(browse_dialog_clist));

	gtk_clist_freeze(GTK_CLIST(browse_dialog_clist));
	for(li = main_object->current_notices; li != NULL; li = li->next)
		browse_dialog_clist_append_notice(li->data);
	gtk_clist_thaw(GTK_CLIST(browse_dialog_clist));
}

/*
 * Notice changes
 */
static void
notice_posted(GrapevineMain *o, GrapevineNoticeReal *notice)
{
	browse_dialog_clist_append_notice(notice);
}

static void
notice_removed(GrapevineMain *o, GrapevineNoticeReal *notice)
{
	int i;
	GList *li;
	for(li = GTK_CLIST(browse_dialog_clist)->row_list, i = 0;
	    li != NULL;
	    li = li->next, i++) {
		GtkCListRow *row = li->data;
		if(row->data == notice) {
			gtk_clist_remove(GTK_CLIST(browse_dialog_clist), i);
			return;
		}
	}
}

static void
notice_changed(GrapevineMain *o, GrapevineNoticeReal *notice)
{
	int i;
	GList *li;
	GtkCList *clist = GTK_CLIST(browse_dialog_clist);
	for(li = clist->row_list, i = 0; li != NULL; li = li->next, i++) {
		GtkCListRow *row = li->data;
		if(row->data == notice) {
			char **t = fill_notice_info_row(notice);
			int j;
			for(j = 0; j < 4; j++)
				gtk_clist_set_text(clist, i, j, t[j]);
			g_strfreev(t);
			return;
		}
	}
}

/*
 * Keyword entry related
 */
static void
keywords_activate(GtkEntry *entry, gpointer data)
{
	GList *list, *li;
	char *p, *s;
	int i;
	g_return_if_fail(GTK_IS_ENTRY(entry));

	list = NULL;
	s = g_strdup(gtk_entry_get_text(entry));
	p = strtok(s, KEYWORD_SEPARATORS);
	while(p) {
		list = g_list_append(list, g_strdup(p));
		p = strtok(NULL, KEYWORD_SEPARATORS);
	}
	g_free(s);

	g_strfreev(browse_keywords);
	browse_keywords = NULL;

	if(list != NULL) {
		browse_keywords = g_new(char *, g_list_length(list)+1);
		for(i = 0, li = list; li != NULL; i++, li = li->next) {
			browse_keywords[i] = li->data;
		}
		browse_keywords[i] = NULL;
		g_list_free(list);
	}

	browse_dialog_clist_fill();
}


/*
 * Clist related
 */

static int
compare_timestamps(GtkCList *clist, gconstpointer ptr1, gconstpointer ptr2)
{
	GtkCListRow *row1 = (GtkCListRow *) ptr1;
	GtkCListRow *row2 = (GtkCListRow *) ptr2;

	GrapevineNoticeReal *not1 = row1->data;
	GrapevineNoticeReal *not2 = row2->data;

	if(not1->timestamp > not2->timestamp)
		return 1;
	else if(not1->timestamp < not2->timestamp)
		return -1;
	else
		return 0;
}

static int
compare_priorities(GtkCList *clist, gconstpointer ptr1, gconstpointer ptr2)
{
	GtkCListRow *row1 = (GtkCListRow *) ptr1;
	GtkCListRow *row2 = (GtkCListRow *) ptr2;

	GrapevineNoticeReal *not1 = row1->data;
	GrapevineNoticeReal *not2 = row2->data;

	if(not1->priority > not2->priority)
		return 1;
	else if(not1->priority < not2->priority)
		return -1;
	else
		return 0;
}

static void
setup_arrows(void)
{
	int i;
	GtkCList *clist = GTK_CLIST(browse_dialog_clist);
	for(i = 0; i < 4; i++) {
		if(i != clist->sort_column)
			gtk_widget_hide(browse_dialog_arrows[i]);
		else {
			gtk_widget_show(browse_dialog_arrows[i]);
			if (clist->sort_type == GTK_SORT_ASCENDING)
				gtk_arrow_set
					(GTK_ARROW(browse_dialog_arrows[i]),
					 GTK_ARROW_DOWN,
					 GTK_SHADOW_OUT);
			else
				gtk_arrow_set
					(GTK_ARROW(browse_dialog_arrows[i]),
					 GTK_ARROW_UP,
					 GTK_SHADOW_OUT);
		}
	}
}

static void 
clist_click_column (GtkCList *clist, gint column, gpointer data)
{
	if(column == clist->sort_column) {
		if (clist->sort_type == GTK_SORT_ASCENDING)
			gtk_clist_set_sort_type(clist, GTK_SORT_DESCENDING);
		else
			gtk_clist_set_sort_type(clist, GTK_SORT_ASCENDING);
	} else
		gtk_clist_set_sort_column (clist, column);

	/* timestamp */
	if(column == 0)
		gtk_clist_set_compare_func(clist, compare_timestamps);
	/* priorities */
	else if(column == 1)
		gtk_clist_set_compare_func(clist, compare_priorities);
	/* strings */
	else
		gtk_clist_set_compare_func(clist, NULL);

	gtk_clist_sort (clist);

	setup_arrows();
}

/*
 * Keyword select dialog stuff
 */

static void 
select_clist_click_column (GtkCList *clist, gint column, GtkWidget *arrow)
{
	if(column == clist->sort_column) {
		if (clist->sort_type == GTK_SORT_ASCENDING)
			gtk_clist_set_sort_type(clist, GTK_SORT_DESCENDING);
		else
			gtk_clist_set_sort_type(clist, GTK_SORT_ASCENDING);
	} else
		gtk_clist_set_sort_column (clist, column);

	gtk_clist_sort (clist);

	if (clist->sort_type == GTK_SORT_ASCENDING)
		gtk_arrow_set (GTK_ARROW(arrow),
			       GTK_ARROW_DOWN,
			       GTK_SHADOW_OUT);
	else
		gtk_arrow_set (GTK_ARROW(arrow),
			       GTK_ARROW_UP,
			       GTK_SHADOW_OUT);

}

static int
compare_strcase(GtkCList *clist, gconstpointer ptr1, gconstpointer ptr2)
{
	GtkCListRow *row1 = (GtkCListRow *) ptr1;
	GtkCListRow *row2 = (GtkCListRow *) ptr2;

	char *s1 = row1->data;
	char *s2 = row2->data;

	return g_strcasecmp(s1, s2);
}

static void
insert_keyword(GtkCList *clist, GHashTable *got_these, char *keyword,
	       gboolean do_select)
{
	if( ! g_hash_table_lookup(got_these, keyword)) {
		char *t[1];
		int row;
		keyword = g_strdup(keyword);
		g_hash_table_insert(got_these, keyword, keyword);
		t[0] = keyword;
		row = gtk_clist_append(clist, t);
		gtk_clist_set_row_data_full(clist, row, g_strdup(keyword),
					    (GtkDestroyNotify)g_free);
		if(do_select)
			gtk_clist_select_row(clist, row, 0);
	}
}

static void
insert_all_keywords(GtkCList *clist, GHashTable *got_these)
{
	GList *li;
	for(li = main_object->current_notices; li != NULL; li = li->next) {
		GrapevineNoticeReal *notice = li->data;
		int i;
		for(i = 0; notice->keywords && notice->keywords[i]; i++) {
			insert_keyword(clist, got_these, notice->keywords[i],
				       FALSE);
		}
	}
}

static void
insert_current_keywords(GtkCList *clist, GHashTable *got_these)
{
	char *s, *p;

	s = g_strdup(gtk_entry_get_text(GTK_ENTRY(browse_keywords_entry)));
	p = strtok(s, KEYWORD_SEPARATORS);
	while(p) {
		insert_keyword(clist, got_these, p, TRUE);
		p = strtok(NULL, KEYWORD_SEPARATORS);
	}
	g_free(s);
}

static void
select_dialog_clicked(GtkWidget *dialog, int button, GtkCList *clist)
{
	GString *gs;
	char *div = "";
	GList *li;
	g_return_if_fail(GTK_IS_CLIST(clist));
	g_return_if_fail(GNOME_IS_DIALOG(dialog));

	if(button != 0) {
		gnome_dialog_close(GNOME_DIALOG(dialog));
		return;
	}

	gs = g_string_new(NULL);

	for(li = clist->row_list; li != NULL; li = li->next) {
		GtkCListRow *row = li->data;
		if(row->state == GTK_STATE_SELECTED) {
			g_string_append(gs, div);
			g_string_append(gs, row->data);
			div = " ";
		}
	}

	gtk_entry_set_text(GTK_ENTRY(browse_keywords_entry), gs->str);

	g_string_free(gs, TRUE);

	gtk_widget_activate(browse_keywords_entry);

	gnome_dialog_close(GNOME_DIALOG(dialog));
}

static void
keyword_select_dialog_show(GtkWidget *widget, gpointer data)
{
	GladeXML *xml;
	GtkWidget *clist, *w;
	GHashTable *got_these;

	xml = glade_helper_load ("grapevine.glade", "keyword_select_dialog",
				 gnome_dialog_get_type (),
				 TRUE /* dump on destroy */);

	keyword_select_dialog =
		dialog_do_dialog(xml, "keyword_select_dialog",
				 GTK_WINDOW(browse_dialog),
				 NULL);
	if( ! keyword_select_dialog)
		return;

	gtk_signal_connect(GTK_OBJECT(keyword_select_dialog), "destroy",
			   GTK_SIGNAL_FUNC(gtk_widget_destroyed),
			   &keyword_select_dialog);
	gnome_dialog_set_close(GNOME_DIALOG(keyword_select_dialog), FALSE);

	clist = glade_helper_get_clist (xml, "clist_select", GTK_TYPE_CLIST, 1);

	w = glade_helper_get (xml, "select_arrow_col1", GTK_TYPE_ARROW);

	gtk_signal_connect(GTK_OBJECT (clist), "click_column",
			   GTK_SIGNAL_FUNC(select_clist_click_column), w);
	gtk_arrow_set (GTK_ARROW(w), GTK_ARROW_DOWN, GTK_SHADOW_OUT);
	gtk_signal_connect(GTK_OBJECT(keyword_select_dialog), "clicked",
			   GTK_SIGNAL_FUNC(select_dialog_clicked),
			   clist);

	gtk_clist_clear(GTK_CLIST(clist));
	gtk_clist_freeze(GTK_CLIST(clist));

	/* hash table for knowing which keywords we already inserted */
	got_these = g_hash_table_new(str_case_hash, str_case_equal);

	insert_current_keywords(GTK_CLIST(clist), got_these);
	insert_all_keywords(GTK_CLIST(clist), got_these);

	g_hash_table_foreach(got_these, (GHFunc)g_free, NULL);
	g_hash_table_destroy(got_these);

	gtk_clist_set_compare_func(GTK_CLIST(clist), compare_strcase);
	gtk_clist_set_sort_column(GTK_CLIST(clist), 0);
	gtk_clist_set_sort_type(GTK_CLIST(clist), GTK_SORT_ASCENDING);
	gtk_clist_sort(GTK_CLIST(clist));
	gtk_clist_thaw(GTK_CLIST(clist));
}
					  

/*
 * Notice dismissal
 */
static void
really_dismiss_notice(gpointer data)
{
	GrapevineNoticeReal *notice = data;

	g_return_if_fail(notice != NULL);
	g_return_if_fail(GRAPEVINE_IS_NOTICE_REAL(notice));

	grapevine_notice_real_remove(notice);
}

static void
dismiss_notice(GtkWidget *widget, gpointer data)
{
	GtkCList *clist = GTK_CLIST(browse_dialog_clist);
	GrapevineNoticeReal *notice;
	GtkWidget *confirm_dlg;
	int row;

	if( ! clist->selection) {
		GtkWidget *w = gnome_error_dialog(_("No notice selected"));
		dialog_setup(w, GTK_WINDOW(browse_dialog), NULL);
		gnome_dialog_run(GNOME_DIALOG(w));
		return;
	}

	row = GPOINTER_TO_INT(clist->selection->data);

	notice = gtk_clist_get_row_data(clist, row);

	g_assert(notice != NULL);

	confirm_dlg = grp_question_dialog(_("Dismiss this notice?"),
					  GRP_QUESTION_YES_NO,
					  really_dismiss_notice,
					  notice,
					  NULL,
					  "/apps/grapevine/dialogs/dismiss",
					  TRUE,
					  GTK_WINDOW(browse_dialog));

	if(confirm_dlg)
		gtk_signal_connect_object_while_alive
			(GTK_OBJECT(notice), "destroy",
			 GTK_SIGNAL_FUNC(gtk_widget_destroy),
			 GTK_OBJECT(confirm_dlg));
}

/*
 * Notice editing
 */
static void
edit_notice(GtkWidget *widget, gpointer data)
{
	GtkWidget *w = gnome_ok_dialog("Not yet implemented");
	dialog_setup(w, GTK_WINDOW(browse_dialog), NULL);
	gnome_dialog_run(GNOME_DIALOG(w));
}

/*
 * View notice
 */
static void
view_notice(GtkWidget *widget, gpointer data)
{
	GtkCList *clist = GTK_CLIST(browse_dialog_clist);
	GrapevineNoticeReal *notice;
	int row;

	if( ! clist->selection) {
		GtkWidget *w = gnome_error_dialog(_("No notice selected"));
		dialog_setup(w, GTK_WINDOW(browse_dialog), NULL);
		gnome_dialog_run(GNOME_DIALOG(w));
		return;
	}

	row = GPOINTER_TO_INT(clist->selection->data);

	notice = gtk_clist_get_row_data(clist, row);

	g_assert(notice != NULL);

	view_show(notice, GTK_WINDOW(browse_dialog));
}

void
browse_dialog_show (void)
{
	GladeXML *xml;
	GtkWidget *w;

	puts("RUNNING BROWSE DIALOG");
	if (browse_dialog != NULL) {
		gtk_widget_show_now(browse_dialog);
		gdk_window_raise(browse_dialog->window);
		return;
	}

	xml = glade_helper_load ("grapevine.glade", "browse_dialog",
				 gnome_dialog_get_type (),
				 TRUE /* dump on destroy */);

	active_views ++;

	browse_dialog = dialog_do_dialog(xml, "browse_dialog", NULL, NULL);
	if( ! browse_dialog)
		return;

	gtk_signal_connect(GTK_OBJECT(browse_dialog), "destroy",
			   GTK_SIGNAL_FUNC(browse_dialog_destroy),
			   NULL);
	gnome_dialog_set_close(GNOME_DIALOG(browse_dialog), TRUE);

	browse_keywords_entry = glade_helper_get (xml, "keywords_entry",
						  GTK_TYPE_ENTRY);

	gtk_signal_connect(GTK_OBJECT(browse_keywords_entry), "activate",
			   GTK_SIGNAL_FUNC(keywords_activate), NULL);

	w = glade_helper_get (xml, "keyword_select_button",
			      GTK_TYPE_BUTTON);
	gtk_signal_connect(GTK_OBJECT(w), "clicked",
			   GTK_SIGNAL_FUNC(keyword_select_dialog_show),
			   NULL);

	w = glade_helper_get (xml, "dismiss_button", GTK_TYPE_BUTTON);
	gtk_signal_connect(GTK_OBJECT(w), "clicked",
			   GTK_SIGNAL_FUNC(dismiss_notice),
			   NULL);

	w = glade_helper_get (xml, "edit_button", GTK_TYPE_BUTTON);
	gtk_signal_connect(GTK_OBJECT(w), "clicked",
			   GTK_SIGNAL_FUNC(edit_notice),
			   NULL);

	w = glade_helper_get (xml, "view_button", GTK_TYPE_BUTTON);
	gtk_signal_connect (GTK_OBJECT (w), "clicked",
			    GTK_SIGNAL_FUNC (view_notice),
			    NULL);

	browse_dialog_clist = glade_helper_get_clist (xml, "clist_browse",
						      GTK_TYPE_CLIST, 4);
	gtk_signal_connect (GTK_OBJECT (browse_dialog_clist), "click_column",
			    GTK_SIGNAL_FUNC (clist_click_column), NULL);

	gtk_clist_set_sort_type (GTK_CLIST (browse_dialog_clist),
				 GTK_SORT_ASCENDING);
	gtk_clist_set_sort_column (GTK_CLIST (browse_dialog_clist), 0);
	gtk_clist_set_compare_func (GTK_CLIST (browse_dialog_clist),
				    compare_timestamps);
	
	browse_dialog_arrows[0] = glade_helper_get (xml, "arrow_col1",
						    GTK_TYPE_ARROW);
	browse_dialog_arrows[1] = glade_helper_get (xml, "arrow_col2",
						    GTK_TYPE_ARROW);
	browse_dialog_arrows[2] = glade_helper_get (xml, "arrow_col3",
						    GTK_TYPE_ARROW);
	browse_dialog_arrows[3] = glade_helper_get (xml, "arrow_col4",
						    GTK_TYPE_ARROW);

	setup_arrows();

	/* we put the browse dialog pointer to data, so that we can easily
	 * disconnect */
	gtk_signal_connect_after(GTK_OBJECT(main_object), "post_notice",
				 GTK_SIGNAL_FUNC(notice_posted),
				 browse_dialog);
	gtk_signal_connect_after(GTK_OBJECT(main_object), "remove_notice",
				 GTK_SIGNAL_FUNC(notice_removed),
				 browse_dialog);
	gtk_signal_connect_after(GTK_OBJECT(main_object), "change_notice",
				 GTK_SIGNAL_FUNC(notice_changed),
				 browse_dialog);

	browse_dialog_clist_fill();
}
