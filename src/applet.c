/* Grapevine: GNOME Notifications
 *   The applet view
 * Author: George Lebl
 * (c) 2000 Eazel, Inc.
 * (c) 2001 George Lebl
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#include "config.h"
#include <gnome.h>
#include <sys/time.h>
#include <unistd.h>

#include <glade/glade.h>
#include <libgnorba/gnorba.h>
#include <applet-widget.h>
#include <gconf/gconf.h>
#include <gconf/gconf-client.h>
#include <pong/pong.h>

#include "gnome-grapevine.h"
#include "grapevine-main.h"
#include "grapevine-notice-real.h"

#include "scroll-menu.h"
#include "grapevine-util.h"
#include "dialog-setup.h"
#include "browse-dialog.h"
#include "view.h"
#include "applet.h"

#include "defines.h"

/*
 * Externs
 */
extern GConfClient *client;
extern PongXML *config;
extern int active_views;
extern GrapevineMain *main_object;

static GdkColor critical_color = { -1, 65535 /*r*/, 0 /*g*/, 0 /*b*/ };
static GdkColor medium_color = { -1, 65535 /*r*/, 43520 /*g*/, 21760 /*b*/ };
static GdkColor medium_rare_color = { -1, 43520 /*r*/, 65535 /*g*/, 43520 /*b*/ };
static GdkColor informational_color = { -1, 65535 /*r*/, 65535 /*g*/, 65535 /*b*/ };

enum {
	FILTER_NO_FILTER,
	FILTER_EXCLUSIVE,
	FILTER_INCLUSIVE
};

enum {
	FILTER_CRITICAL = 1<<0,
	FILTER_MEDIUM = 1<<1,
	FILTER_MEDIUM_RARE = 1<<2,
	FILTER_INFORMATIONAL = 1<<3
};

typedef struct _GrapevineApplet GrapevineApplet;
struct _GrapevineApplet {
	AppletWidget *applet;

	char *config_prefix;
	PongXML *config;

	GtkWidget *ebox;
	GtkWidget *hbox;
	GtkWidget *vbox;
	GtkWidget *frame;
	GtkWidget *arrow;
	GtkWidget *arrow_button;
	GtkWidget *button_h;
	GtkWidget *label_h;
	GtkWidget *button_v;
	GtkWidget *label_v;

	int criticals;
	int mediums;
	int medium_rares;
	int informationals;

	int number;

	PanelOrientType orient;
	int size;

	GtkWidget *aboutbox;

	GtkWidget *popup;
	GrapevineNoticeReal *popup_notice;
	guint popup_timeout;

	guint gconf_notify;
	guint filter_change_idle_id;
};

typedef struct _Filter Filter;
struct _Filter {
	int keyword_filter_style;
	GSList *keywords;
	guint priorities;
};

/*
 * Filter functions
 */
static Filter *
filter_make (const GrapevineApplet *ga)
{
	char path[256];
	Filter *filter;
	char *foo;
	GSList *list, *li;
	GError *error = NULL;

	g_return_val_if_fail (ga != NULL, NULL);

	filter = g_new0 (Filter, 1);

	g_snprintf (path, sizeof (path),
		    "/apps/grapevine-applet/%s/filter/keyword_filter_style",
		    ga->config_prefix);
	foo = gconf_client_get_string (client, path, &error);
	GRP_HANDLE_GCONF_ERROR (;);

	if (foo != NULL &&
	    strcmp (foo, "NO_FILTER") == 0)
		filter->keyword_filter_style = FILTER_NO_FILTER;
	else if (foo != NULL &&
		 strcmp (foo, "EXCLUSIVE") == 0)
		filter->keyword_filter_style = FILTER_EXCLUSIVE;
	else if (foo != NULL &&
		 strcmp (foo, "INCLUSIVE") == 0)
		filter->keyword_filter_style = FILTER_INCLUSIVE;
	else {
		g_warning ("Invalid keyword filter style '%s', "
			   "assuming NO_FILTER", foo ? foo : "(null)");
		filter->keyword_filter_style = FILTER_NO_FILTER;
	}

	g_free (foo);

	g_snprintf (path, sizeof (path),
		    "/apps/grapevine-applet/%s/filter/priorities",
		    ga->config_prefix);
	list = gconf_client_get_list (client, path, GCONF_VALUE_STRING,
				      &error);
	GRP_HANDLE_GCONF_ERROR (;);

	filter->priorities = 0;
	for (li = list; li != NULL; li = li->next) {
		char *priority = li->data;
		if (priority == NULL) {
			g_warning ("Weird, not a NULL from get_list");
			/*something is weird*/
			continue;
		}
		if (strcmp (priority, "CRITICAL") == 0)
			filter->priorities |= FILTER_CRITICAL;
		else if (strcmp (priority, "MEDIUM") == 0)
			filter->priorities |= FILTER_MEDIUM;
		else if (strcmp (priority, "MEDIUM_RARE") == 0)
			filter->priorities |= FILTER_MEDIUM_RARE;
		else if (strcmp (priority, "INFORMATIONAL") == 0)
			filter->priorities |= FILTER_INFORMATIONAL;
		else
			g_warning ("Invalid filter priority %s", priority);
	}

	grp_slist_free_deep (list);

	g_snprintf (path, sizeof (path),
		    "/apps/grapevine-applet/%s/filter/keywords",
		    ga->config_prefix);
	filter->keywords = gconf_client_get_list (client, path,
						  GCONF_VALUE_STRING, &error);
	GRP_HANDLE_GCONF_ERROR (;);

	return filter;
}

static void
filter_destroy (Filter *filter)
{
	g_return_if_fail (filter != NULL);

	if (filter->keywords != NULL) {
		grp_slist_free_deep (filter->keywords);
		filter->keywords = NULL;
	}
	g_free (filter);
}

static gboolean
filter_notice_matches (Filter *filter, GrapevineNoticeReal *real)
{
	g_return_val_if_fail (filter != NULL, FALSE);
	g_return_val_if_fail (real != NULL, FALSE);
	g_return_val_if_fail (GRAPEVINE_IS_NOTICE_REAL (real), FALSE);

	if ((real->priority == GRAPEVINE_CRITICAL &&
	     ! (filter->priorities & FILTER_CRITICAL)) ||
	    (real->priority == GRAPEVINE_MEDIUM &&
	     ! (filter->priorities & FILTER_MEDIUM)) ||
	    (real->priority == GRAPEVINE_MEDIUM_RARE &&
	     ! (filter->priorities & FILTER_MEDIUM_RARE)) ||
	    (real->priority == GRAPEVINE_INFORMATIONAL &&
	     ! (filter->priorities & FILTER_INFORMATIONAL)))
		return FALSE;

	if (filter->keyword_filter_style == FILTER_EXCLUSIVE) {
		GSList *li;
		for (li = filter->keywords; li != NULL; li = li->next) {
			char *keyword = li->data;
			if (grapevine_notice_real_has_keyword (real, keyword))
				return FALSE;
		}
	} else if (filter->keyword_filter_style == FILTER_INCLUSIVE) {
		GSList *li;
		gboolean a_match = FALSE;
		for (li = filter->keywords; li != NULL; li = li->next) {
			char *keyword = li->data;
			if (grapevine_notice_real_has_keyword (real, keyword)) {
				a_match = TRUE;
				break;
			}
		}
		
		if ( ! a_match)
			return FALSE;
	}

	return TRUE;
}


/*
 * Menu action handlers
 */
static void
browse_cb (AppletWidget *widget, gpointer data)
{
	browse_dialog_show ();
}

static void
grapevine_properties_cb (AppletWidget *widget, gpointer data)
{
	if (config != NULL) {
		pong_xml_show_dialog (config);
	} else {
		config = grapevine_properties_show ("gnome-grapevine.pong",
						    NULL);

		/* Most likely the dialog has already taken ref of the
		 * PongXML object, and we want to keep it around as long
		 * as the dialog is around */
		gtk_signal_connect (GTK_OBJECT (config), "destroy",
				    GTK_SIGNAL_FUNC (gtk_widget_destroyed),
				    &config);

		/* loose our ref, we only keep a weak reference */
		gtk_object_unref (GTK_OBJECT (config));
	}
}

static void
properties_cb (AppletWidget *widget, gpointer data)
{
	GrapevineApplet *ga = data;
	if (ga->config != NULL) {
		pong_xml_show_dialog (ga->config);
	} else {
		ga->config = grapevine_properties_show
			("gnome-grapevine-applet.pong", ga->config_prefix);

		/* Most likely the dialog has already taken ref of the
		 * PongXML object, and we want to keep it around as long
		 * as the dialog is around */
		gtk_signal_connect_while_alive
			(GTK_OBJECT (ga->config), "destroy",
			 GTK_SIGNAL_FUNC (gtk_widget_destroyed),
			 &ga->config,
			 GTK_OBJECT (ga->applet));

		/* loose our ref, we only keep a weak reference */
		gtk_object_unref (GTK_OBJECT (ga->config));
	}
}

/*the most important dialog in the whole application*/
static void
about_cb (AppletWidget *widget, gpointer data)
{
	GrapevineApplet *ga = data;
	char *logo;
	char *authors[] = {
		"George Lebl <jirka@5z.com>",
		NULL
	};

	if (ga->aboutbox != NULL) {
		gtk_widget_show (ga->aboutbox);
		gdk_window_raise (ga->aboutbox->window);
		return;
	}

	logo = gnome_pixmap_file ("gnome-grapevine/grapevine-sign.png");

	if (logo == NULL &&
	    g_file_exists (PIXMAPDIR "/grapevine-sign.png")) {
		logo = g_strdup (PIXMAPDIR "/grapevine-sign.png");
	}

	ga->aboutbox =
		gnome_about_new (_("Grapevine Applet"), VERSION,
				 "(C) 2000 Eazel, Inc., (C) 2001 George Lebl",
				 (const char **)authors,
				 _("Grapevine is the GNOME notification "
				   "framework, it is responsible for "
				   "notifying you of different events, "
				   "managing and logging these notifications "
				   "and making your coffee."),
				 logo);
	g_free (logo);
	dialog_setup (ga->aboutbox, NULL, NULL);
	/*gnome_window_icon_set_from_file (GTK_WINDOW (ga->aboutbox),
					 GNOME_ICONDIR"/gnome-grapevine.png");
					 */
	gtk_signal_connect (GTK_OBJECT (ga->aboutbox), "destroy",
			    GTK_SIGNAL_FUNC (gtk_widget_destroyed),
			    &ga->aboutbox);
	gtk_widget_show (ga->aboutbox);
}

/*
 * Applet functions
 */
static void
set_colors_to (GrapevineApplet *ga, GdkColor *color)
{
	set_widget_color (ga->ebox, color);
	set_widget_color (ga->hbox, color);
	set_widget_color (ga->vbox, color);
	/* Frame should be in panel colors */
	/*set_widget_color (ga->frame, color);*/
	set_widget_color (ga->arrow, color);
	set_widget_color (ga->arrow_button, color);
	set_widget_color (ga->button_h, color);
	set_widget_color (ga->label_h, color);
	set_widget_color (ga->button_v, color);
	set_widget_color (ga->label_v, color);

	gtk_widget_queue_draw (ga->frame);
}

static void
unset_colors (GrapevineApplet *ga)
{
	gtk_widget_set_rc_style (ga->ebox);
	gtk_widget_set_rc_style (ga->hbox);
	gtk_widget_set_rc_style (ga->vbox);
	/* Frame should be in panel colors */
	/*gtk_widget_set_rc_style (ga->frame);*/
	gtk_widget_set_rc_style (ga->arrow);
	gtk_widget_set_rc_style (ga->arrow_button);
	gtk_widget_set_rc_style (ga->button_h);
	gtk_widget_set_rc_style (ga->label_h);
	gtk_widget_set_rc_style (ga->button_v);
	gtk_widget_set_rc_style (ga->label_v);

	gtk_widget_queue_draw (ga->frame);
}

static void
setup_styles (GrapevineApplet *ga)
{
	if (ga->criticals > 0) {
		set_colors_to (ga, &critical_color);
	} else if (ga->mediums > 0) {
		set_colors_to (ga, &medium_color);
	} else if (ga->medium_rares > 0) {
		set_colors_to (ga, &medium_rare_color);
	} else if (ga->informationals > 0) {
		set_colors_to (ga, &informational_color);
	} else {
		unset_colors (ga);
	}
}

static void
draw_applet (GrapevineApplet *ga)
{
	char *s;

	g_return_if_fail (ga != NULL);

	s = g_strdup_printf ("%d", ga->number);
	gtk_label_set (GTK_LABEL (ga->label_h), s);
	gtk_label_set (GTK_LABEL (ga->label_v), s);
	g_free (s);

	setup_styles (ga);
}

static void
popup_destroyed (GtkWidget *dialog, GrapevineApplet *ga)
{
	ga->popup = NULL;
	if (ga->popup_notice != NULL) {
		gtk_object_unref (GTK_OBJECT (ga->popup_notice));
		ga->popup_notice = NULL;
	}
	if (ga->popup_timeout != 0) {
		gtk_timeout_remove (ga->popup_timeout);
		ga->popup_timeout = 0;
	}
}

static gboolean
destroy_popup (gpointer data)
{
	GrapevineApplet *ga = data;
	ga->popup_timeout = 0;
	if (ga->popup != NULL) {
		gtk_widget_destroy (ga->popup);
		ga->popup = NULL;
	}

	return FALSE;
}

static gboolean
popup_button_press (GtkWidget *popup, GdkEventButton *event, gpointer data)
{
	GrapevineApplet *ga = data;

	if (ga->popup_notice != NULL)
		view_show (ga->popup_notice, NULL);

	if (ga->popup != NULL)
		gtk_widget_destroy (ga->popup);

	return FALSE;
}

static void
popup_notice (GrapevineApplet *ga, GrapevineNoticeReal *notice)
{
	GtkWidget *vbox, *frame;
	GtkWidget *w;
	char path[256];
	gboolean flash_new;
	int flash_new_time;
	GError *error = NULL;
	GdkColor *color;

	g_snprintf (path, sizeof (path),
		    "/apps/grapevine-applet/%s/ui/flash_new",
		    ga->config_prefix);
	flash_new = gconf_client_get_bool (client, path, &error);
	GRP_HANDLE_GCONF_ERROR (;);

	if ( ! flash_new)
		 return;

	if (ga->popup != NULL) {
		gtk_widget_destroy (ga->popup);
		ga->popup = NULL;
	}

	g_assert (ga->popup_notice == NULL);

	if (notice->priority == GRAPEVINE_CRITICAL) 
		color = &critical_color;
	else if (notice->priority == GRAPEVINE_MEDIUM) 
		color = &medium_color;
	else if (notice->priority == GRAPEVINE_MEDIUM_RARE) 
		color = &medium_rare_color;
	else /* if (notice->priority == GRAPEVINE_INFORMATIONAL) */
		color = &informational_color;

	ga->popup_notice = notice;
	gtk_object_ref (GTK_OBJECT (ga->popup_notice));

	ga->popup = gtk_widget_new (GTK_TYPE_WINDOW,
				    "type", GTK_WINDOW_POPUP,
				    "title", _("Grapevine New Notice"),
				    "auto_shrink", FALSE,
				    "allow_shrink", FALSE,
				    "allow_grow", TRUE,
				    "events", GDK_BUTTON_PRESS_MASK | GDK_BUTTON_RELEASE_MASK,
				    "signal::destroy", popup_destroyed, ga,
				    "signal::button_press_event", popup_button_press, ga,
				    NULL);
	set_widget_color (ga->popup, color);

	frame = gtk_widget_new (GTK_TYPE_FRAME,
				"parent", ga->popup,
				"shadow", GTK_SHADOW_OUT,
				NULL);
	set_widget_color (frame, color);

	vbox = gtk_widget_new (GTK_TYPE_VBOX,
			       "spacing", (int)GNOME_PAD_SMALL,
			       "homogeneous", (gboolean)FALSE,
			       "parent", frame,
			       NULL);
	set_widget_color (vbox, color);

	if (notice->progress >= 0) {
		char *message = g_strdup_printf ("%s (%d%%)", notice->summary,
						 notice->progress);
		w = gtk_label_new (message);
		g_free (message);
	} else {
		w = gtk_label_new (notice->summary);
	}
	set_widget_color (w, color);
	gtk_box_pack_start (GTK_BOX (vbox), w, TRUE, TRUE, 0);
	
	gtk_widget_show_all (frame);

	gtk_widget_size_request (ga->popup, NULL);
	gtk_widget_realize (ga->popup);

	/* foo? */
	gtk_container_check_resize (GTK_CONTAINER (ga->popup));
	if (ga->applet != NULL &&
	    GTK_WIDGET_REALIZED (ga->applet)) {
		int x, y;
		int scrw, scrh;

		scrw = gdk_screen_width ();
		scrh = gdk_screen_height ();

		gdk_window_get_origin (GTK_WIDGET (ga->applet)->window, &x, &y);

		x += GTK_WIDGET (ga->applet)->allocation.width / 2;
		y += GTK_WIDGET (ga->applet)->allocation.height / 2;

		x -= ga->popup->allocation.width / 2;
		y -= ga->popup->allocation.height / 2;

		if (x + ga->popup->allocation.width > scrw)
			x = scrw - ga->popup->allocation.width;
		else if (x < 0)
			x = 0;
		if (y + ga->popup->allocation.height > scrh)
			y = scrh - ga->popup->allocation.height;
		else if (y < 0)
			y = 0;

		gtk_widget_set_uposition (ga->popup, x, y);
	}

	gtk_widget_show (ga->popup);
	gdk_flush ();

	gnome_win_hints_set_state (GTK_WIDGET (ga->popup),
				  WIN_STATE_STICKY | WIN_STATE_HIDDEN);
	gnome_win_hints_set_layer (GTK_WIDGET (ga->popup),
				   WIN_LAYER_ABOVE_DOCK);
	gnome_win_hints_set_hints (GTK_WIDGET (ga->popup),
				   WIN_HINTS_DO_NOT_COVER);

	gdk_window_raise (ga->popup->window);

	g_snprintf (path, sizeof (path),
		    "/apps/grapevine-applet/%s/ui/flash_new_time",
		    ga->config_prefix);
	flash_new_time = gconf_client_get_int (client, path, &error);
	GRP_HANDLE_GCONF_ERROR (;);

	if (flash_new_time < 1 ||
	    flash_new_time > 999) {
		g_warning (_("Flash time out of range, using 3 seconds"));
		flash_new_time = 3;
	}

	ga->popup_timeout = gtk_timeout_add (flash_new_time * 1000,
					     destroy_popup, ga);
}


/*
 * Applet stuff
 */
static int
applet_save_session (GtkWidget *w,
		     const char *privcfgpath,
		     const char *globcfgpath,
		     GrapevineApplet *ga)
{
	gnome_config_push_prefix (privcfgpath);

	gnome_config_set_string ("gconf/config_prefix", ga->config_prefix);

	gnome_config_pop_prefix ();

	gnome_config_sync ();
	gnome_config_drop_all ();

	return FALSE;
}

static void
applet_destroy (GtkWidget *applet, GrapevineApplet *ga)
{
	puts ("DESTROY APPLET");

	if (ga->popup_timeout != 0)
		gtk_timeout_remove (ga->popup_timeout);
	ga->popup_timeout = 0;
	if (ga->popup != NULL)
		gtk_widget_destroy (ga->popup);
	ga->popup = NULL;

	if (ga->filter_change_idle_id != 0) {
		gtk_idle_remove (ga->filter_change_idle_id);
		ga->filter_change_idle_id = 0;
	}

	if (ga->gconf_notify != 0) {
		gconf_client_notify_remove (client, ga->gconf_notify);
		ga->gconf_notify = 0;
	}

	gtk_signal_disconnect_by_data (GTK_OBJECT (main_object), ga);

	g_free (ga);

	active_views --;
	gtk_main_quit ();
}

static void
applet_change_orient (GtkWidget *w, PanelOrientType o, gpointer data)
{
	GrapevineApplet *ga = data;

	ga->orient = o;

	if (o == ORIENT_UP || o == ORIENT_DOWN) {
		gtk_widget_show (ga->button_v);
		gtk_widget_hide (ga->button_h);
	} else {
		gtk_widget_hide (ga->button_v);
		gtk_widget_show (ga->button_h);
	}

	if (o == ORIENT_UP)
		gtk_arrow_set (GTK_ARROW (ga->arrow),
			       GTK_ARROW_UP, GTK_SHADOW_OUT);
	else if (o == ORIENT_DOWN)
		gtk_arrow_set (GTK_ARROW (ga->arrow),
			       GTK_ARROW_DOWN, GTK_SHADOW_OUT);
	else if (o == ORIENT_LEFT)
		gtk_arrow_set (GTK_ARROW (ga->arrow),
			       GTK_ARROW_LEFT, GTK_SHADOW_OUT);
	else if (o == ORIENT_RIGHT)
		gtk_arrow_set (GTK_ARROW (ga->arrow),
			       GTK_ARROW_RIGHT, GTK_SHADOW_OUT);
}

static void
applet_change_pixel_size (GtkWidget *w, int size, gpointer data)
{
	GrapevineApplet *ga = data;

	ga->size = size;

	/* FIXME: FOO */
}

/*
 * Notice changes
 */
static void
notice_posted (GrapevineMain *o, GrapevineNoticeReal *notice,
	       GrapevineApplet *ga)
{
	Filter *filter;
	filter = filter_make (ga);
	if (filter_notice_matches (filter, notice)) {
		ga->number ++;
		if (notice->priority == GRAPEVINE_CRITICAL) 
			ga->criticals ++;
		else if (notice->priority == GRAPEVINE_MEDIUM) 
			ga->mediums ++;
		else if (notice->priority == GRAPEVINE_MEDIUM_RARE) 
			ga->medium_rares ++;
		else /* if (notice->priority == GRAPEVINE_INFORMATIONAL) */
			ga->informationals ++;
		popup_notice (ga, notice);
		draw_applet (ga);
	}
	filter_destroy (filter);
}


static void
notice_removed (GrapevineMain *o, GrapevineNoticeReal *notice,
		GrapevineApplet *ga)
{
	Filter *filter;
	filter = filter_make (ga);
	if (filter_notice_matches (filter, notice)) {
		ga->number --;
		if (notice->priority == GRAPEVINE_CRITICAL) 
			ga->criticals --;
		else if (notice->priority == GRAPEVINE_MEDIUM) 
			ga->mediums --;
		else if (notice->priority == GRAPEVINE_MEDIUM_RARE) 
			ga->medium_rares --;
		else /* if (notice->priority == GRAPEVINE_INFORMATIONAL) */
			ga->informationals --;

		if (ga->popup_notice == notice && ga->popup)
			gtk_widget_destroy (ga->popup);
		draw_applet (ga);
	}
	filter_destroy (filter);
}

/*
 * Display functions
 */

static gboolean
destroy_menu (gpointer data)
{
	GtkWidget *menu = data;
	gtk_widget_destroy (menu);

	/* kill the idle */
	return FALSE;
}

static void
button_no_leave (GtkWidget *w, gpointer data)
{
	gtk_widget_set_state (w, GTK_STATE_ACTIVE);
	gtk_widget_queue_draw (w);
}


static void
menu_deactivate (GtkWidget *w, GrapevineApplet *ga)
{
	gtk_idle_add (destroy_menu, w);

	gtk_signal_disconnect_by_func (GTK_OBJECT (ga->arrow_button),
				       button_no_leave, NULL);
	gtk_button_released (GTK_BUTTON (ga->arrow_button));
}

static void
notice_menu_position (GtkMenu *menu, gint *x, gint *y, gpointer data)
{
	GrapevineApplet *ga = data;
	int wx, wy;
	GtkRequisition menu_req;
	GtkRequisition button_req;

	gdk_window_get_origin (ga->arrow_button->window, &wx, &wy);

	gtk_widget_get_child_requisition (GTK_WIDGET (menu), &menu_req);
	gtk_widget_get_child_requisition (GTK_WIDGET (ga->arrow_button),
					  &button_req);

	switch (ga->orient) {
	case ORIENT_UP:
		*x = wx;
		*y = wy - menu_req.height;
		break;
	case ORIENT_DOWN:
		*x = wx;
		*y = wy + button_req.height;
		break;
	case ORIENT_LEFT:
		*x = wx - menu_req.width;
		*y = wy;
		break;
	case ORIENT_RIGHT:
		*x = wx + button_req.width;
		*y = wy;
		break;
	default:
		break;
	}
	if (*x + menu_req.width > gdk_screen_width ())
		*x = gdk_screen_width () - menu_req.width;
	if (*y + menu_req.height > gdk_screen_height ())
		*y = gdk_screen_height () - menu_req.height;
	if (*x < 0)
		*x = 0;
	if (*y < 0)
		*y = 0;
}

static void
browse_notices (GtkWidget *w, gpointer data)
{
	browse_dialog_show ();
}

static void
view_notice (GtkWidget *w, gpointer data)
{
	GrapevineNoticeReal *notice = data;

	view_show (notice, NULL);
}

static void
unref_notice (GtkWidget *w, gpointer data)
{
	GrapevineNoticeReal *notice = data;

	gtk_object_unref (GTK_OBJECT (notice));
}

static void
display_notice_menu (GtkWidget *button, GrapevineApplet *ga)
{
	GtkWidget *menu;
	GtkWidget *w;
	GList *li;
	int added = 0;
	Filter *filter;

	g_assert (main_object != NULL);

	menu = scroll_menu_new ();

	w = gtk_menu_item_new_with_label (_("Browse notices..."));
	gtk_signal_connect (GTK_OBJECT (w), "activate",
			    GTK_SIGNAL_FUNC (browse_notices),
			    NULL);
	gtk_widget_show (w);
	gtk_menu_append (GTK_MENU (menu), w);

	w = gtk_menu_item_new ();
	gtk_widget_show (w);
	gtk_widget_set_sensitive (w, FALSE);
	gtk_menu_append (GTK_MENU (menu), w);

	filter = filter_make (ga);

	for (li = main_object->current_notices; li != NULL; li = li->next) {
		GrapevineNoticeReal *notice = li->data;
		char *s;
		const char *summary;
		const char *priority;

		if ( ! filter_notice_matches (filter, notice))
			continue;

		/* FIXME: should be icons with colors and stuff */
		if (notice->priority == GRAPEVINE_CRITICAL) 
			priority = _("critical");
		else if (notice->priority == GRAPEVINE_MEDIUM) 
			priority = _("medium");
		else if (notice->priority == GRAPEVINE_MEDIUM_RARE) 
			priority = _("medium rare");
		else /* if (notice->priority == GRAPEVINE_INFORMATIONAL) */
			priority = _("informational");

		if (notice->summary != NULL)
			summary = notice->summary;
		else
			summary = _("No summary");

		if (notice->progress >= 0)
			s = g_strdup_printf ("%s (%d%%) [%s]",
					     summary,
					     notice->progress,
					     priority);
		else
			s = g_strdup_printf ("%s [%s]",
					     summary,
					     priority);

		w = gtk_menu_item_new_with_label (s);
		gtk_widget_show (w);

		gtk_object_ref (GTK_OBJECT (notice));

		gtk_signal_connect (GTK_OBJECT (w), "activate",
				    GTK_SIGNAL_FUNC (view_notice),
				    notice);
		gtk_signal_connect (GTK_OBJECT (w), "destroy",
				    GTK_SIGNAL_FUNC (unref_notice),
				    notice);

		gtk_menu_append (GTK_MENU (menu), w);

		added ++;
	}

	filter_destroy (filter);
	filter = NULL;

	if (added == 0) {
		w = gtk_menu_item_new_with_label (_("No notices"));
		gtk_widget_show (w);
		gtk_widget_set_sensitive (w, FALSE);

		gtk_menu_append (GTK_MENU (menu), w);
	}

	{
		GdkEventButton *bevent =
			(GdkEventButton*)gtk_get_current_event ();

		gtk_menu_popup (GTK_MENU (menu),
				NULL,
				NULL,
				notice_menu_position,
				ga,
				bevent->button, bevent->time);
	}

	gtk_signal_connect_after (GTK_OBJECT (menu), "deactivate",
				  GTK_SIGNAL_FUNC (menu_deactivate),
				  ga);

	gtk_grab_remove (button);

	gtk_signal_connect_after (GTK_OBJECT (button), "leave",
				  GTK_SIGNAL_FUNC (button_no_leave),
				  NULL);
}

static void
display_latest_notice (GtkWidget *w, GrapevineApplet *ga)
{
	GrapevineNoticeReal *notice;
	notice = grapevine_main_get_latest_posted_notice (main_object);
	if (notice != NULL)
		view_show (notice, NULL);
}

/*
 * Coutning the notice
 * Does not do redraw
 */
static void
count_notices (GrapevineApplet *ga)
{
	GList *li;
	Filter *filter;

	g_assert (main_object != NULL);

	filter = filter_make (ga);

	ga->number = 0;
	ga->criticals = 0;
	ga->mediums = 0;
	ga->medium_rares = 0;
	ga->informationals = 0;

	for (li = main_object->current_notices; li != NULL; li = li->next) {
		GrapevineNoticeReal *notice = li->data;

		if (filter_notice_matches (filter, notice)) {
			ga->number ++;
			if (notice->priority == GRAPEVINE_CRITICAL) 
				ga->criticals ++;
			else if (notice->priority == GRAPEVINE_MEDIUM) 
				ga->mediums ++;
			else if (notice->priority == GRAPEVINE_MEDIUM_RARE) 
				ga->medium_rares ++;
			else /* if (notice->priority == GRAPEVINE_INFORMATIONAL) */
				ga->informationals ++;
		}
	}

	filter_destroy (filter);
}

/* Load all values from default location and setting them onto
 * the location plus ga->config_prefix */
static void
load_and_set_defaults (GrapevineApplet *ga)
{
	const char *keys[] = {
		"ui/flash_new",
		"ui/flash_new_time",
		"filter/keyword_filter_style",
		"filter/keywords",
		"filter/priorities",
		NULL
	};
	int i;
	GError *error = NULL;


	for (i = 0; keys[i] != NULL; i++) {
		char path[256];
		GConfValue *val;
		g_snprintf (path, sizeof (path), "/apps/grapevine-applet/%s",
			    keys[i]);
		val = gconf_client_get (client, path, &error);
		GRP_HANDLE_GCONF_ERROR (;);
		if (val != NULL) {
			g_snprintf (path, sizeof (path), "/apps/grapevine-applet/%s/%s",
				    ga->config_prefix,
				    keys[i]);
			gconf_client_set (client, path, val, &error);
			GRP_HANDLE_GCONF_ERROR (;);
			gconf_value_free (val);
		}
	}
}

static gboolean
filter_change_idle (gpointer data)
{
	GrapevineApplet *ga = data;

	g_return_val_if_fail (ga != NULL, FALSE);

	count_notices (ga);
	draw_applet (ga);

	ga->filter_change_idle_id = 0;

	return FALSE;
}

static void
filter_change (GConfClient* client, guint cnxn_id,
	      GConfEntry* entry, gpointer user_data)
{
	GrapevineApplet *ga = user_data;

	g_return_if_fail (ga != NULL);

	if (ga->filter_change_idle_id != 0) {
		ga->filter_change_idle_id = gtk_idle_add (filter_change_idle, ga);
	}
}

static void
theme_change (GtkWidget *w, GtkStyle *old_style, gpointer data)
{
	GrapevineApplet *ga = data;

	setup_styles (ga);
}

static GrapevineApplet *
create_applet (const char *goad_id)
{
	GrapevineApplet *ga;
	GtkWidget *w;
	char path[256];
	GError *error = NULL;

	puts ("CREATE APPLET");

	ga = g_new0 (GrapevineApplet, 1);

	ga->size = PIXEL_SIZE_STANDARD;
	ga->orient = ORIENT_UP;
	ga->filter_change_idle_id = 0;

	w = applet_widget_new (goad_id);
	if (w == NULL) {
		g_warning (_("Can't create applet"));
		return NULL;
	}
	ga->applet = APPLET_WIDGET (w);

	gnome_config_push_prefix (ga->applet->privcfgpath);
	ga->config_prefix = gnome_config_get_string ("gconf/config_prefix=");
	gnome_config_pop_prefix ();

	if (ga->config_prefix == NULL ||
	    *ga->config_prefix == '\0') {
		struct timeval tv;

		gettimeofday (&tv, NULL);

		g_free (ga->config_prefix);
		ga->config_prefix = g_strdup_printf ("%da%ldb%ld", (int) rand (),
						     (long) tv.tv_sec, (long) tv.tv_usec);

		load_and_set_defaults (ga);
	}

	gtk_signal_connect (GTK_OBJECT (ga->applet), "change_orient",
			    GTK_SIGNAL_FUNC (applet_change_orient),
			    ga);
	gtk_signal_connect (GTK_OBJECT (ga->applet), "change_pixel_size",
			    GTK_SIGNAL_FUNC (applet_change_pixel_size),
			    ga);

	gtk_signal_connect (GTK_OBJECT (ga->applet), "style_set",
			    GTK_SIGNAL_FUNC (theme_change),
			    ga);

	ga->ebox = gtk_event_box_new ();

	ga->vbox = gtk_vbox_new (FALSE, 0);
	ga->hbox = gtk_hbox_new (FALSE, 0);

	ga->frame = gtk_frame_new (NULL);
	gtk_frame_set_shadow_type (GTK_FRAME (ga->frame), GTK_SHADOW_IN);
	gtk_container_add (GTK_CONTAINER (ga->frame), ga->ebox);
	gtk_container_add (GTK_CONTAINER (ga->ebox), ga->vbox);
	gtk_box_pack_start (GTK_BOX (ga->vbox), ga->hbox, FALSE, FALSE, 0);

	ga->arrow = gtk_arrow_new (GTK_ARROW_UP, GTK_SHADOW_OUT);
	ga->arrow_button = gtk_button_new ();
	gtk_button_set_relief (GTK_BUTTON (ga->arrow_button),
			       GTK_RELIEF_NONE);
	gtk_container_add (GTK_CONTAINER (ga->arrow_button), ga->arrow);
	gtk_box_pack_start (GTK_BOX (ga->hbox), ga->arrow_button, TRUE, TRUE, 0);
	gtk_signal_connect_after (GTK_OBJECT (ga->arrow_button), "pressed",
				  GTK_SIGNAL_FUNC (display_notice_menu),
				  ga);

	ga->label_h = gtk_label_new ("??");
	ga->button_h = gtk_button_new ();
	gtk_button_set_relief (GTK_BUTTON (ga->button_h),
			       GTK_RELIEF_NONE);
	gtk_container_add (GTK_CONTAINER (ga->button_h), ga->label_h);
	gtk_box_pack_start (GTK_BOX (ga->hbox), ga->button_h, TRUE, TRUE, 0);
	gtk_signal_connect (GTK_OBJECT (ga->button_h), "clicked",
			    GTK_SIGNAL_FUNC (display_latest_notice),
			    ga);

	ga->label_v = gtk_label_new ("??");
	ga->button_v = gtk_button_new ();
	gtk_button_set_relief (GTK_BUTTON (ga->button_v),
			       GTK_RELIEF_NONE);
	gtk_container_add (GTK_CONTAINER (ga->button_v), ga->label_v);
	gtk_box_pack_start (GTK_BOX (ga->vbox), ga->button_v, FALSE, FALSE, 0);
	gtk_signal_connect (GTK_OBJECT (ga->button_v), "clicked",
			    GTK_SIGNAL_FUNC (display_latest_notice),
			    ga);

	gtk_widget_show_all (ga->frame);

	gtk_widget_hide (ga->button_h);
	gtk_widget_hide (ga->button_v);

	applet_widget_add (APPLET_WIDGET (ga->applet), ga->frame);
	gtk_widget_show (GTK_WIDGET (ga->applet));

	gtk_signal_connect (GTK_OBJECT (ga->applet), "save_session",
			    GTK_SIGNAL_FUNC (applet_save_session), ga);
	gtk_signal_connect (GTK_OBJECT (ga->applet), "destroy",
			    GTK_SIGNAL_FUNC (applet_destroy), ga);

	applet_widget_register_callback (APPLET_WIDGET (ga->applet),
					 "browse",
					 _("Browse Notices..."),
					 browse_cb,
					 ga);
	applet_widget_register_callback (APPLET_WIDGET (ga->applet),
				 	 "grapevine_properties",
					 _("Global Grapevine Preferences..."),
					 grapevine_properties_cb,
					 ga);
	applet_widget_register_callback (APPLET_WIDGET (ga->applet),
					 "properties",
					 _("Properties..."),
					 properties_cb,
					 ga);
	applet_widget_register_stock_callback (APPLET_WIDGET (ga->applet),
					       "about",
					       GNOME_STOCK_MENU_ABOUT,
					       _("About..."),
					       about_cb,
					       ga);

	gtk_signal_connect_after (GTK_OBJECT (main_object), "post_notice",
				  GTK_SIGNAL_FUNC (notice_posted), ga);
	gtk_signal_connect_after (GTK_OBJECT (main_object), "remove_notice",
				  GTK_SIGNAL_FUNC (notice_removed), ga);

	g_snprintf (path, sizeof (path),
		    "/apps/grapevine-applet/%s/filter",
		    ga->config_prefix);
	ga->gconf_notify = gconf_client_notify_add (client, path,
						    filter_change, ga, NULL,
						    &error);
	GRP_HANDLE_GCONF_ERROR (;);


	count_notices (ga);

	draw_applet (ga);

	active_views++;

	return ga;
}

static gboolean
applet_query_function (const char *goad_id)
{
	if (strcmp (goad_id, "gnome_grapevine_applet") == 0)
		return TRUE;
	else
		return FALSE;
}

static GtkWidget *
applet_activate_function (const char *goad_id,
			  const char **params,
			  int nparams)
{
	GrapevineApplet *ga;

	if (strcmp (goad_id, "gnome_grapevine_applet") != 0)
		return NULL;

	ga = create_applet (goad_id);

	if (ga == NULL)
		return NULL;

	return GTK_WIDGET (ga->applet);
}

void
applet_add_factory (void)
{
	applet_factory_new ("gnome_grapevine", applet_query_function,
			    applet_activate_function);
}
