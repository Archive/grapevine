/* Grapevine: GNOME Notifications
 *   Utility functions
 * Author: George Lebl
 * (c) 2000 Eazel, Inc.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#include "config.h"
#include <gnome.h>
#include <glade/glade.h>
#include <gconf/gconf.h>
#include <gconf/gconf-client.h>
#include <time.h>
#include <pong/pong.h>

#include "grapevine-util.h"
#include "dialog-setup.h"

/*
 * Externs
 */
extern GConfClient *client;
extern int active_views;

/* count length of a vector */
int
grp_strlengthv (char **argv)
{
	int i = 0;
	if (argv != NULL) {
		while (argv[i] != NULL)
			i++;
	}
	return i;
}

/* if argc is -1, then argv is taken as a NULL terminated vector */
char **
grp_strdupv (char ** argv, int argc)
{
	char **ret = NULL;

	if (argv != NULL) {
		int i;

		if (argc < 0)
			argc = grp_strlengthv (argv);
		ret = g_new (char *, argc + 1);

		for (i = 0; i < argc; i++)
			ret[i] = g_strdup (argv[i]);

		ret[i] = NULL;
	}

	return ret;
}

/* gets a unique ID string, each call will give a different string */
char *
grp_create_unique_id (void)
{
	static int uniq = 0;
	GTimeVal tv;
	int rnd = random ();

	if (uniq == 0)
		uniq = random ();

	g_get_current_time (&tv);

	/* This will get a very likely completely unique ID */
	return g_strdup_printf ("%Xx%Xy%Xz%X",
				(int) (uniq ++), /* A unique per-instance number */
				(int) rnd, /* Random number */
				(int) tv.tv_sec, /* Current time, secs */
				(int) tv.tv_usec /* cur time, usecs */);
}

typedef struct {
	int ref_count;
	GtkWidget *toggle;
	char *gconf_key;
	GrpCallback func;
	gpointer data;
	GDestroyNotify destroy_notify;
} QuestionDlg;

static void
q_dialog_ref (QuestionDlg *d)
{
	d->ref_count++;
}
static void
q_dialog_unref (QuestionDlg *d)
{
	d->ref_count--;
	if (d->ref_count == 0) {
		if (d->destroy_notify != NULL)
			d->destroy_notify (d->data);
		d->destroy_notify = NULL;
		d->data = NULL;
		g_free (d->gconf_key);
		d->gconf_key = NULL;
		g_free (d);
	}
}

/* Function is always called in an idle, this makes sure that
 * there is no dialog anymore which could interfere with some
 * refcounting/destruction issues */

static gboolean
q_idle_handler (gpointer data)
{
	QuestionDlg *d = data;
	d->func (d->data);
	q_dialog_unref (d);
	return FALSE;
}

static void
q_callback (int reply, gpointer data)
{
	QuestionDlg *d = data;
	if (reply == 0) {
		q_dialog_ref (d);
		gtk_idle_add (q_idle_handler, d);
	}
}

static void
dlg_destroyed (GtkWidget *w, gpointer data)
{
	QuestionDlg *d = data;

	g_return_if_fail (d != NULL);

	/* This had to be active to begin with.  so only change state
	 * (turn off) when it's not active */
	if ( ! GTK_TOGGLE_BUTTON (d->toggle)->active) {
		GError *error = NULL;
		gconf_client_set_bool (client, d->gconf_key, FALSE, &error);
		GRP_HANDLE_GCONF_ERROR (;);
	}
	q_dialog_unref (d);
}

GtkWidget *
grp_question_dialog (const char *question,
		     int dlg_type,
		     GrpCallback yes_callback,
		     gpointer data,
		     GDestroyNotify destroy_notify,
		     const char *gconf_show_key,
		     gboolean yes_when_no_dialog,
		     GtkWindow *parent)
{
	QuestionDlg *d;
	GError *error = NULL;
	gboolean showdlg;
	GtkWidget *dlg;

	g_return_val_if_fail (question != NULL, NULL);
	g_return_val_if_fail (yes_callback != NULL, NULL);
	g_return_val_if_fail (gconf_show_key != NULL, NULL);
	g_return_val_if_fail (dlg_type == GRP_QUESTION_YES_NO ||
			      dlg_type == GRP_QUESTION_OK_CANCEL, NULL);

	showdlg = gconf_client_get_bool (client, gconf_show_key, &error);
	GRP_HANDLE_GCONF_ERROR (showdlg = TRUE);

	d = g_new0 (QuestionDlg, 1);
	d->ref_count = 1;
	d->func = yes_callback;
	d->data = data;
	d->destroy_notify = destroy_notify;

	if ( ! showdlg) {
		if (yes_when_no_dialog)
			gtk_idle_add (q_idle_handler, d);
		else
			q_dialog_unref (d);
		return NULL;
	}

	d->gconf_key = g_strdup (gconf_show_key);

	if (dlg_type == GRP_QUESTION_YES_NO) {
		dlg = gnome_question_dialog (question, q_callback, d);
	} else /* if (dlg_type == GRP_QUESTION_OK_CANCEL) */ {
		dlg = gnome_ok_cancel_dialog (question, q_callback, d);
	}

	dialog_setup (dlg, parent, NULL);

	d->toggle = gtk_check_button_new_with_label
		(_("Show this dialog in the future"));
	gtk_toggle_button_set_state (GTK_TOGGLE_BUTTON (d->toggle), TRUE);

	gtk_box_pack_end (GTK_BOX (GNOME_DIALOG (dlg)->vbox),
			  d->toggle, FALSE, FALSE, 0);

	gtk_signal_connect (GTK_OBJECT (dlg), "destroy",
			    GTK_SIGNAL_FUNC (dlg_destroyed), d);

	gtk_widget_show_all (dlg);

	return dlg;
}
		    
gboolean
grp_question_dialog_modal (const char *question,
			   int dlg_type,
			   const char *gconf_show_key,
			   gboolean yes_when_no_dialog,
			   GtkWindow *parent)
{
	GError *error = NULL;
	gboolean ret, showdlg;
	GtkWidget *dlg;
	GtkWidget *w;

	g_return_val_if_fail (question != NULL, FALSE);
	g_return_val_if_fail (gconf_show_key != NULL, FALSE);
	g_return_val_if_fail (dlg_type == GRP_QUESTION_YES_NO ||
			      dlg_type == GRP_QUESTION_OK_CANCEL, FALSE);

	showdlg = gconf_client_get_bool (client, gconf_show_key, &error);
	GRP_HANDLE_GCONF_ERROR (showdlg = TRUE);

	if ( ! showdlg)
		return yes_when_no_dialog;

	if (dlg_type == GRP_QUESTION_YES_NO) {
		dlg = gnome_question_dialog_modal (question,
						  NULL, NULL);
	} else /* if (dlg_type == GRP_QUESTION_OK_CANCEL) */ {
		dlg = gnome_ok_cancel_dialog_modal (question,
						    NULL, NULL);
	}

	dialog_setup (dlg, parent, NULL);

	w = gtk_check_button_new_with_label
		(_("Show this dialog in the future"));
	gtk_toggle_button_set_state (GTK_TOGGLE_BUTTON (w), TRUE);

	gtk_box_pack_end (GTK_BOX (GNOME_DIALOG (dlg)->vbox),
			  w, FALSE, FALSE, 0);

	gtk_widget_show_all (dlg);

	ret = FALSE;
	if (gnome_dialog_run_and_close (GNOME_DIALOG (dlg)) == 0) {
		ret = TRUE;
	}

	if ( ! GTK_TOGGLE_BUTTON (w)->active) {
		gconf_client_set_bool (client, gconf_show_key, FALSE, &error);
		GRP_HANDLE_GCONF_ERROR (;);
	}

	return ret;
}

static void
msg_dlg_destroyed (GtkWidget *w, gpointer data)
{
	char *gconf_show_key = data;
	GtkWidget *toggle = gtk_object_get_user_data (GTK_OBJECT (w));

	g_return_if_fail (gconf_show_key != NULL);
	g_return_if_fail (toggle != NULL);
	g_return_if_fail (GTK_IS_TOGGLE_BUTTON (toggle));

	/* This had to be active to begin with.  so only change state
	 * (turn off) when it's not active */
	if ( ! GTK_TOGGLE_BUTTON (toggle)->active) {
		GError *error = NULL;
		gconf_client_set_bool (client, gconf_show_key, FALSE, &error);
		GRP_HANDLE_GCONF_ERROR (;);
	}
	g_free (gconf_show_key);
}

GtkWidget *
grp_msg_dialog (const char *msg,
		int dlg_type,
		const char *gconf_show_key,
		GtkWindow *parent)
{
	GError *error = NULL;
	gboolean showdlg;
	GtkWidget *dlg, *w;

	g_return_val_if_fail (msg != NULL, NULL);
	g_return_val_if_fail (gconf_show_key != NULL, NULL);
	g_return_val_if_fail (dlg_type == GRP_MSG_OK, NULL);

	showdlg = gconf_client_get_bool (client, gconf_show_key, &error);
	GRP_HANDLE_GCONF_ERROR (showdlg = TRUE);

	if ( ! showdlg)
		return NULL;

	/*if (dlg_type == GRP_MSG_OK)*/
	dlg = gnome_ok_dialog (msg);
	dialog_setup (dlg, parent, NULL);

	w = gtk_check_button_new_with_label
		(_("Show this dialog in the future"));
	/* set the toggle button as the user data of the dialog */
	gtk_object_set_user_data (GTK_OBJECT (dlg), w);
	gtk_toggle_button_set_state (GTK_TOGGLE_BUTTON (w), TRUE);

	gtk_box_pack_end (GTK_BOX (GNOME_DIALOG (dlg)->vbox),
			  w, FALSE, FALSE, 0);

	gtk_signal_connect (GTK_OBJECT (dlg), "destroy",
			    GTK_SIGNAL_FUNC (msg_dlg_destroyed), 
			    g_strdup (gconf_show_key));

	gtk_widget_show_all (dlg);

	return dlg;
}

typedef struct _Alarm Alarm;
struct _Alarm {
	unsigned long alarm;
	guint gtk_id;
	GrpAlarmFunc function;
	gpointer data;

	GDestroyNotify destroy_notify;
	gboolean immediate;
};

static guint minute_tick_id = 0;
static GList *alarms = NULL;

static gboolean
precise_handler (gpointer data)
{
	Alarm *a = data;
	a->function (a->data);

	if (a->destroy_notify != NULL)
		a->destroy_notify (a->data);
	a->destroy_notify = NULL;
	a->data = NULL;

	g_free (a);
	return FALSE;
}

static gboolean
minute_tick (gpointer data)
{
	GList *li;
	unsigned long cur_time = time (NULL);
	li = alarms;
	while (li != NULL) {
		Alarm *a = li->data;
		if ((a->alarm - cur_time) <= 60) {
			GList *tmp;
			a->gtk_id = gtk_timeout_add ((a->alarm - cur_time) * 1000, precise_handler, a);
			tmp = li->next;
			if (li->prev != NULL)
				li->prev->next = li->next;
			else
				alarms = li->next;
			if (li->next != NULL)
				li->next->prev = li->prev;
			g_list_free_1 (li);
			li = tmp;
		} else {
			li = li->next;
		}
	}
	if ( ! alarms) {
		minute_tick_id = 0;
		return FALSE;
	} else
		return TRUE;
}

static gboolean
immediate_handler (gpointer data)
{
	Alarm *a = data;
	a->function (a->data);

	if (a->destroy_notify != NULL)
		a->destroy_notify (a->data);
	a->destroy_notify = NULL;
	a->data = NULL;

	g_free (a);
	return FALSE;
}


/* timeouts with precision to seconds */
/* but a bit ugly.  The returned pointer can
 * only be used while the alarm is active, thus you have
 * to set up a destroy_notify and make sure you never call
 * grp_alarm_remove after it has been destroyed */
gpointer
grp_alarm_add (unsigned long alarm,
	       GrpAlarmFunc function,
	       GDestroyNotify destroy_notify,
	       gpointer data)
{
	Alarm *a;
	unsigned long cur_time = time (NULL);


	a = g_new0 (Alarm, 1);
	a->immediate = FALSE;
	a->alarm = alarm ;
	a->gtk_id = 0;
	a->function = function;
	a->destroy_notify = destroy_notify;
	a->data = data;

	if (alarm <= cur_time) {
		a->immediate = TRUE;
		a->gtk_id = g_idle_add (immediate_handler, a);
	} else if ((a->alarm - cur_time) <= 60) {
		a->gtk_id = gtk_timeout_add ((a->alarm - cur_time) * 1000,
					     precise_handler, a);
	} else {
		alarms = g_list_prepend (alarms, a);
		if (minute_tick_id == 0)
			minute_tick_id = gtk_timeout_add (60 * 1000,
							  minute_tick, NULL);
	}

	return a;
}

void
grp_alarm_remove (gpointer handle)
{
	Alarm *a = handle;

	if (handle == NULL)
		return;

	if ( ! a->immediate) {
		if (a->gtk_id != 0)
			gtk_timeout_remove (a->gtk_id);
		else
			alarms = g_list_remove (alarms, a);
	} else if (a->gtk_id != 0) {
		gtk_idle_remove (a->gtk_id);
	}
	a->gtk_id = 0;

	if (a->destroy_notify != NULL)
		a->destroy_notify (a->data);
	a->destroy_notify = NULL;
	a->data = NULL;
	g_free (a);
}

static void
config_dialog_destroyed (GtkWidget *dialog, gpointer data)
{
	active_views --;
	gtk_main_quit ();
}

PongXML *
grapevine_properties_show (const char *file, const char *prefix)
{
	GtkWidget *dialog;
	PongXML *config;

	config = pong_xml_new (file);
	if (config == NULL) {
		char *s;
		GtkWidget *w;
		s = g_strdup_printf (_("Grapevine: Cannot load PonG file: %s"),
				     file);
		w = gnome_error_dialog (s);
		g_free (s);
		dialog_setup (w, NULL, NULL);
		return NULL;
	}
	if (prefix != NULL)
		pong_xml_set_prefix (config, prefix);
	pong_xml_show_dialog (config);

	dialog = pong_xml_get_dialog_widget (config);
	if (dialog != NULL) {
		active_views ++;
		gtk_signal_connect (GTK_OBJECT (dialog), "destroy",
				    GTK_SIGNAL_FUNC (config_dialog_destroyed),
				    NULL);
		dialog_setup (dialog, NULL, NULL);
	}

	/* The callee should unref the config */
	return config;
}

void
grp_play_sound (const char *data, int length)
{
	char template[] = "/tmp/grapevine-XXXXXX";
	int fd;

	if (data == NULL)
		return;

	fd = mkstemp (template);
	if (fd < 0)
		return;

	write (fd, data, length);

	close (fd);

	gnome_sound_play (template);

	unlink (template);
}

/* Do strcasecmp but ignore locale, usefull for parsing where locale would
 * just do damage */
int
grp_strcasecmp_no_locale (const char *s1, const char *s2)
{
	int i;

	/* Error, but don't make them equal then */
	g_return_val_if_fail (s1 != NULL, G_MAXINT);
	g_return_val_if_fail (s2 != NULL, G_MININT);

	for (i = 0; s1[i] != '\0' && s2[i] != '\0'; i++) {
		char a = s1[i];
		char b = s2[i];

		if (a >= 'A' && a <= 'Z')
			a -= 'A' - 'a';
		if (b >= 'A' && b <= 'Z')
			b -= 'A' - 'a';

		if (a < b)
			return -1;
		else if (a > b)
			return 1;
	}

	/* find out which string is smaller */
	if (s2[i] != '\0')
		return -1; /* s1 is smaller */
	else if (s1[i] != '\0')
		return 1; /* s2 is smaller */
	else
		return 0; /* equal */
}

const char *
grp_sure_string (const char *s)
{
	if (s == NULL)
		return "";
	else
		return s;
}

void
grp_list_free_deep (GList *list)
{
	GList *li;

	for (li = list; li != NULL; li = li->next) {
		g_free (li->data);
		li->data = NULL;
	}

	if (list != NULL)
		g_list_free (list);
}

void
grp_slist_free_deep (GSList *list)
{
	GSList *li;

	for (li = list; li != NULL; li = li->next) {
		g_free (li->data);
		li->data = NULL;
	}

	if (list != NULL)
		g_slist_free (list);
}

gboolean
grp_string_empty (const char *s)
{
	if (s == NULL || *s == '\0')
		return TRUE;
	else
		return FALSE;
}


/*following code shamelessly stolen from gtk*/
static void
rgb_to_hls (gdouble *r,
	    gdouble *g,
	    gdouble *b)
{
  gdouble min;
  gdouble max;
  gdouble red;
  gdouble green;
  gdouble blue;
  gdouble h, l, s;
  gdouble delta;

  red = *r;
  green = *g;
  blue = *b;

  if (red > green)
    {
      if (red > blue)
	max = red;
      else
	max = blue;

      if (green < blue)
	min = green;
      else
	min = blue;
    }
  else
    {
      if (green > blue)
	max = green;
      else
	max = blue;

      if (red < blue)
	min = red;
      else
	min = blue;
    }

  l = (max + min) / 2;
  s = 0;
  h = 0;

  if (max != min)
    {
      if (l <= 0.5)
	s = (max - min) / (max + min);
      else
	s = (max - min) / (2 - max - min);

      delta = max -min;
      if (red == max)
	h = (green - blue) / delta;
      else if (green == max)
	h = 2 + (blue - red) / delta;
      else if (blue == max)
	h = 4 + (red - green) / delta;

      h *= 60;
      if (h < 0.0)
	h += 360;
    }

  *r = h;
  *g = l;
  *b = s;
}

static void
hls_to_rgb (gdouble *h,
	    gdouble *l,
	    gdouble *s)
{
  gdouble hue;
  gdouble lightness;
  gdouble saturation;
  gdouble m1, m2;
  gdouble r, g, b;

  lightness = *l;
  saturation = *s;

  if (lightness <= 0.5)
    m2 = lightness * (1 + saturation);
  else
    m2 = lightness + saturation - lightness * saturation;
  m1 = 2 * lightness - m2;

  if (saturation == 0)
    {
      *h = lightness;
      *l = lightness;
      *s = lightness;
    }
  else
    {
      hue = *h + 120;
      while (hue > 360)
	hue -= 360;
      while (hue < 0)
	hue += 360;

      if (hue < 60)
	r = m1 + (m2 - m1) * hue / 60;
      else if (hue < 180)
	r = m2;
      else if (hue < 240)
	r = m1 + (m2 - m1) * (240 - hue) / 60;
      else
	r = m1;

      hue = *h;
      while (hue > 360)
	hue -= 360;
      while (hue < 0)
	hue += 360;

      if (hue < 60)
	g = m1 + (m2 - m1) * hue / 60;
      else if (hue < 180)
	g = m2;
      else if (hue < 240)
	g = m1 + (m2 - m1) * (240 - hue) / 60;
      else
	g = m1;

      hue = *h - 120;
      while (hue > 360)
	hue -= 360;
      while (hue < 0)
	hue += 360;

      if (hue < 60)
	b = m1 + (m2 - m1) * hue / 60;
      else if (hue < 180)
	b = m2;
      else if (hue < 240)
	b = m1 + (m2 - m1) * (240 - hue) / 60;
      else
	b = m1;

      *h = r;
      *l = g;
      *s = b;
    }
}

static void
gtk_style_shade (GdkColor *a,
		 GdkColor *b,
		 gdouble   k)
{
  gdouble red;
  gdouble green;
  gdouble blue;

  red = (gdouble) a->red / 65535.0;
  green = (gdouble) a->green / 65535.0;
  blue = (gdouble) a->blue / 65535.0;

  rgb_to_hls (&red, &green, &blue);

  green *= k;
  if (green > 1.0)
    green = 1.0;
  else if (green < 0.0)
    green = 0.0;

  blue *= k;
  if (blue > 1.0)
    blue = 1.0;
  else if (blue < 0.0)
    blue = 0.0;

  hls_to_rgb (&red, &green, &blue);

  b->red = red * 65535.0;
  b->green = green * 65535.0;
  b->blue = blue * 65535.0;
}

#define LIGHTNESS_MULT  1.3
#define DARKNESS_MULT   0.7

void
set_widget_color (GtkWidget *widget, GdkColor *color)
{
	GtkStyle *ns;
	int i;

	gtk_widget_set_rc_style (widget);
	ns = gtk_style_copy (gtk_widget_get_style (widget));

	ns->bg[GTK_STATE_NORMAL] = *color;
	gtk_style_shade (color, &ns->bg[GTK_STATE_PRELIGHT], 1.5);
	gtk_style_shade (color, &ns->bg[GTK_STATE_ACTIVE], 0.8);
	ns->bg[GTK_STATE_INSENSITIVE] = *color;
	ns->bg[GTK_STATE_SELECTED] = *color;

	for (i = 0; i < 5; i++) {
		gtk_style_shade (&ns->bg[i], &ns->light[i], LIGHTNESS_MULT);
		gtk_style_shade (&ns->bg[i], &ns->dark[i], DARKNESS_MULT);

		ns->text[i] = ns->black;
		ns->fg[i] = ns->black;

		ns->base[i] = *color;

		ns->mid[i].red = (ns->light[i].red + ns->dark[i].red) / 2;
		ns->mid[i].green = (ns->light[i].green + ns->dark[i].green) / 2;
		ns->mid[i].blue = (ns->light[i].blue + ns->dark[i].blue) / 2;

		ns->bg_pixmap[i] = NULL;
		/* FIXME: figure out if this is a leak and unref in this case */
	}

	gtk_widget_set_style (widget, ns);
	gtk_style_unref (ns);
}

guint
str_case_hash(gconstpointer a)
{
	guint ret;
	char *s = g_strdup(a);
	g_strdown(s);
	ret = g_str_hash(s);
	g_free(s);
	return ret;
}

gboolean
str_case_equal(gconstpointer a, gconstpointer b)
{
	return g_strcasecmp(a, b) == 0;
}

