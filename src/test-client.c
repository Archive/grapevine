/* Grapevine: GNOME Notifications
 *   Test client
 * Author: George Lebl
 * (c) 2000 Eazel, Inc.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#include "config.h"
#include <gnome.h>

#include <glade/glade.h>
#include <liboaf/liboaf.h>

#include <bonobo.h>

#include "gnome-grapevine.h"

#include "defines.h"

#include "grapevine-oafiid.h"

int
main(int argc, char *argv[])
{
	CORBA_ORB orb;
	CORBA_Environment ev;
	BonoboObjectClient *notification;
	GNOME_Grapevine_Notification corba_notification;
	GNOME_Grapevine_Notice notice;
	int i;

	bindtextdomain(PACKAGE, GNOMELOCALEDIR);
	textdomain(PACKAGE);
	
	CORBA_exception_init(&ev);


        gnome_init_with_popt_table("grapevine-test-client", VERSION, 
				   argc, argv, oaf_popt_options, 0, NULL);

	orb = oaf_init (argc, argv);
	
	bonobo_init (orb, CORBA_OBJECT_NIL, CORBA_OBJECT_NIL);

	bonobo_activate();

	notification = bonobo_object_activate(GRAPEVINE_NOTIFICATION_OAFIID, 0);
	if(!notification) {
		puts("foo, no notification");
		exit(1);
	}

	corba_notification =
		bonobo_object_corba_objref(BONOBO_OBJECT(notification));

	GNOME_Grapevine_Notification_runBrowseDialog(corba_notification, &ev);

	/*Grapevine_Notification_find_notice(corba_notification,
					   "Just testing",
					   &ev);*/
	for(i = 0; i < 10; i++) {
		char *s;

		notice = GNOME_Grapevine_Notification_createNotice(corba_notification,
							      "Grapevine/TestClient",
							      &ev);
		if( ! CORBA_Object_is_nil(notice, &ev)) {
			GNOME_Grapevine_Notice__set_priority(notice,
						       GNOME_Grapevine_MEDIUM_RARE,
						       &ev);
			s = g_strdup_printf("GEGL %d", i);
			GNOME_Grapevine_Notice__set_summary(notice, s, &ev);
			g_free(s);
			s = g_strdup_printf("GEGL\nGEGL\nGEGL\n(%d)", i);
			GNOME_Grapevine_Notice__set_content(notice, s, &ev);
			g_free(s);
			GNOME_Grapevine_Notice__set_expireIn(notice, 10, &ev);
			GNOME_Grapevine_Notice_post(notice, &ev);

			s = GNOME_Grapevine_Notice__get_uniqueID(notice, &ev);
			printf("Unique ID: '%s'\n", s);
			notice = GNOME_Grapevine_Notification_findNotice(corba_notification, s, &ev);
			if( ! CORBA_Object_is_nil(notice, &ev)) {
				s = GNOME_Grapevine_Notice__get_summary(notice, &ev);
				printf("Found! Summary '%s'\n", s);
			} else {
				printf("Not found!\n");
			}
		}
	}
	
	bonobo_main();

	CORBA_exception_free(&ev);
	
	return 0;
}
