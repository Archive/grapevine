/* Grapevine: GNOME Notifications
 *   Single notice view dialog
 * Author: George Lebl
 * (c) 2000 Eazel, Inc.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#ifndef VIEW_H
#define VIEW_H

/* Avoid circular headers */
#ifndef __TYPEDEF_GRAPEVINE_NOTICE_REAL__
#define __TYPEDEF_GRAPEVINE_NOTICE_REAL__
typedef struct _GrapevineNoticeReal GrapevineNoticeReal;
#endif

void		view_show	(GrapevineNoticeReal *notice,
				 GtkWindow *parent);

#endif /* VIEW_H */
