%at{
/* PonG: Listing of Keywords Entry PonG Widget
 * Author: George Lebl
 * (c) 2001 George Lebl
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.	 See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */
%}
requires 1.0.7

%h{
#include <pong/pong.h>
#include <pong/pong-list-entry.h>
%}

%{
#include "config.h"
#include <gnome.h>

#include <pong/pong-widget-interface.h>

#include "grapevine-util.h"
#include "grapevine-main.h"
#include "grapevine-notice-real.h"

#include "grapevine-keywords-list.h"
#include "grapevine-keywords-list-private.h"

/*
 * Externs
 */
extern int active_views;
extern GrapevineMain *main_object;

/*
 * Local globals
 */
typedef void (*AddOptionsFunc) (GtkWidget *w, GList *pong_options);
static AddOptionsFunc list_add_options = NULL;

%}

class Grapevine:Keywords:List from Pong:List:Entry {

	private GHashTable *keywords
		= {g_hash_table_new (str_case_hash, str_case_equal)}
		destroy {
			g_hash_table_foreach (VAR, (GHFunc)g_free, NULL);
			g_hash_table_destroy (VAR);
		};

	private GList *options = NULL
		destroy {
			if (VAR != NULL) {
				/* keyword strings are in the hash table */
				g_list_foreach (VAR, (GFunc)g_free, NULL);
				g_list_free (VAR);
			}
		};

	private guint post_connection = 0
		destroy {
			if (VAR != 0) {
				gtk_signal_disconnect (GTK_OBJECT (main_object),
						       VAR);
			}
		};
	private guint remove_connection = 0
		destroy {
			if (VAR != 0) {
				gtk_signal_disconnect (GTK_OBJECT (main_object),
						       VAR);
			}
		};

	public GtkWidget *
	new (void)
	{
		guint c;
		GrapevineKeywordsList *self = GET_NEW;

		fill_options (self);

		list_add_options (GTK_WIDGET (self), self->_priv->options);

		c = gtk_signal_connect_after (GTK_OBJECT (main_object),
					      "post_notice",
					      GTK_SIGNAL_FUNC (post_notice),
					      self);
		self->_priv->post_connection = c;
		c = gtk_signal_connect_after (GTK_OBJECT (main_object),
					      "remove_notice",
					      GTK_SIGNAL_FUNC (remove_notice),
					      self);
		self->_priv->remove_connection = c;

		return (GtkWidget *) self;
	}

	class_init (klass)
	{
		PongWidgetInterface *iface;

		iface = pong_widget_interface_add (GTK_OBJECT_CLASS (klass));

		/* remove implementation for add_options, but keep us a
		 * pointer to it */
		list_add_options = iface->add_options;
		iface->add_options = NULL;
	}

	private void
	prepend_keyword (self, const char *keyword (check null))
	{
		if ( ! g_hash_table_lookup (self->_priv->keywords, keyword)) {
			PongOption *option = g_new0 (PongOption, 1);
			char *word = g_strdup (keyword);

			option->label = word;
			option->value = word;

			self->_priv->options =
				g_list_prepend (self->_priv->options, option);

			g_hash_table_insert (self->_priv->keywords, word, word);
		}
	}

	private void
	add_notice_keywords (self,
			     Grapevine:Notice:Real *notice (check null type))
	{
		int i;
		for(i = 0; notice->keywords && notice->keywords[i]; i++) {
			prepend_keyword (self, notice->keywords[i]);
		}
	}

	private void
	clear_options (self)
	{
		g_hash_table_foreach (self->_priv->keywords, (GHFunc)g_free, NULL);
		g_hash_table_destroy (self->_priv->keywords);
		self->_priv->keywords =
			g_hash_table_new (str_case_hash, str_case_equal);

		if (self->_priv->options != NULL) {
			/* keyword strings are in the hash table */
			g_list_foreach (self->_priv->options,
					(GFunc)g_free, NULL);
			g_list_free (self->_priv->options);
			self->_priv->options = NULL;
		}
	}

	private void
	fill_options (self)
	{
		GList *li;
		for (li = main_object->current_notices;
		     li != NULL;
		     li = li->next) {
			GrapevineNoticeReal *notice = li->data;

			add_notice_keywords (self, notice);
		}
	}

	private void
	post_notice (Grapevine:Main *main_object (check null type),
		     Grapevine:Notice:Real *notice (check null type),
		     Grapevine:Keywords:List *self (check null type))
	{
		add_notice_keywords (self, notice);
		list_add_options (GTK_WIDGET (self), self->_priv->options);
	}

	private guint redo_options_idle_id = 0
		destroy {
			if (VAR != 0) {
				gtk_idle_remove (VAR);
			}
		};

	private gboolean
	redo_options_idle (gpointer data)
	{
		Self *self = SELF (data);
		self->_priv->redo_options_idle_id = 0;

		clear_options (self);
		fill_options (self);
		list_add_options (GTK_WIDGET (self), self->_priv->options);

		return FALSE;
	}


	private void
	remove_notice (Grapevine:Main *main_object (check null type),
		       Grapevine:Notice:Real *notice (check null type),
		       Grapevine:Keywords:List *self (check null type))
	{
		if (self->_priv->redo_options_idle_id != 0) {
			self->_priv->redo_options_idle_id =
				gtk_idle_add (redo_options_idle, self);
		}
	}
}
