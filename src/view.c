/* Grapevine: GNOME Notifications
 *   Single notice view dialog
 * Author: George Lebl
 * (c) 2000 Eazel, Inc.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#include "config.h"
#include <gnome.h>

#include <glade/glade.h>
#include <gtkhtml/gtkhtml-stream.h>
#include <gtkhtml/gtkhtml.h>

#include "grapevine-main.h"
#include "grapevine-notice-real.h"

#include "grapevine-util.h"
#include "dialog-setup.h"
#include "glade-helper.h"
#include <X11/Xlib.h>
#include <X11/Xmd.h>
#include <X11/Xatom.h>
#include <gdk/gdkx.h>

#include "view.h"

/*
 * Externs
 */
extern int active_views;
extern GrapevineMain *main_object;

static void
view_dialog_destroy (GtkWidget *dialog, GladeXML *xml)
{
	GrapevineNoticeReal *notice;

	notice = gtk_object_get_user_data (GTK_OBJECT (dialog));

	g_return_if_fail (notice != NULL);
	g_return_if_fail (GRAPEVINE_IS_NOTICE_REAL (notice));

	puts ("VIEW_DIALOG_DESTROY");
	gtk_signal_disconnect_by_data (GTK_OBJECT (notice), xml);

	gtk_object_unref (GTK_OBJECT (notice));

	active_views --;
	gtk_main_quit ();
}

static void
really_dismiss_notice (gpointer _data)
{
	gpointer *data = _data;
	GrapevineNoticeReal *notice;
	GtkWidget *dialog;

	g_return_if_fail (data != NULL);

	notice = data[0];
	dialog = data[1];

	g_return_if_fail (notice != NULL);
	g_return_if_fail (GRAPEVINE_IS_NOTICE_REAL (notice));
	g_return_if_fail (dialog != NULL);
	g_return_if_fail (GNOME_IS_DIALOG (dialog));

	/* remove the notice */
	grapevine_notice_real_remove (notice);

	/* Close the view dialog */
	gnome_dialog_close (GNOME_DIALOG (dialog));
}

static void
view_dialog_clicked (GtkWidget *dialog, int button, GladeXML *xml)
{
	GrapevineNoticeReal *notice;
	GtkWidget *w;
	gpointer *data;

	notice = gtk_object_get_user_data (GTK_OBJECT (dialog));

	g_return_if_fail (notice != NULL);
	g_return_if_fail (GRAPEVINE_IS_NOTICE_REAL (notice));

	switch (button) {
	case 0:
		w = gnome_ok_dialog ("Not yet implemented");
		dialog_setup (w, GTK_WINDOW (dialog), GTK_OBJECT (dialog));
		gnome_dialog_run (GNOME_DIALOG (w));
		break;
	case 1:
		if (notice->state == GRAPEVINE_NOTICE_DEAD) {
			w = gnome_warning_dialog (_("Notice already dead"));
			dialog_setup (w, GTK_WINDOW (dialog),
				      GTK_OBJECT (dialog));
			break;
		}

		data = g_new (gpointer, 2);
		data[0] = notice;
		data[1] = dialog;
		w = grp_question_dialog (_("Dismiss this notice?"),
					 GRP_QUESTION_YES_NO,
					 really_dismiss_notice,
					 data,
					 (GtkDestroyNotify)g_free,
					 "/apps/grapevine/dialogs/dismiss",
					 TRUE,
					 GTK_WINDOW (dialog));
		if (w != NULL) {
			gtk_signal_connect_object_while_alive
				(GTK_OBJECT (notice), "destroy",
				 GTK_SIGNAL_FUNC (gtk_widget_destroy),
				 GTK_OBJECT (w));
		}

		break;
	default:
	case 2:
		gnome_dialog_close (GNOME_DIALOG (dialog));
		break;
	}
}

/*
 * Notice display
 */
static void
my_html_write_string (GtkHTML *html, GtkHTMLStream *stream,
		      const char *string)
{
	g_return_if_fail (string != NULL);

	gtk_html_write (html, stream, string, strlen (string));
}

static void
view_setup_view (GladeXML *xml, GrapevineNoticeReal *notice)
{
	GtkWidget *dialog;
	GtkWidget *gtkhtml;
	GtkHTMLStream *stream;
	char buf[256];
	struct tm *tm;
	time_t the_time;
	char *s;

	/* Note, this has to be in sync with the IDL */
	char *priorities[] = {
		N_("Critical"),
		N_("Medium"),
		N_("Medium Rare"),
		N_("Informational")
	};

	puts ("UPDATING VIEW DIALOG");

	dialog = glade_helper_get (xml, "view_notice_dialog",
				   gnome_dialog_get_type ());

	gtk_window_set_title (GTK_WINDOW (dialog),
			      notice->summary ?
			        notice->summary :
				_("No summary"));

	gtkhtml = gtk_object_get_data (GTK_OBJECT (dialog), "gtkhtml");
	stream = gtk_html_begin (GTK_HTML (gtkhtml));

	my_html_write_string (GTK_HTML (gtkhtml), stream,
			      "<HTML>\n<BODY");
	switch (notice->priority) {
	case GRAPEVINE_CRITICAL:
		my_html_write_string (GTK_HTML (gtkhtml), stream,
				      " TEXT=\"#ffffff\" BGCOLOR=\"#ff0000\"");
		break;
	case GRAPEVINE_MEDIUM:
		my_html_write_string (GTK_HTML (gtkhtml), stream,
				      " TEXT=\"#000000\" BGCOLOR=\"#ffaa55\"");
		break;
	case GRAPEVINE_MEDIUM_RARE:
		my_html_write_string (GTK_HTML (gtkhtml), stream,
				      " TEXT=\"#000000\" BGCOLOR=\"#aaffaa\"");
		break;
	case GRAPEVINE_INFORMATIONAL:
		my_html_write_string (GTK_HTML (gtkhtml), stream,
				      " TEXT=\"#000000\" BGCOLOR=\"#ffffff\"");
		break;
	default:
		break;
	}
	my_html_write_string (GTK_HTML (gtkhtml), stream, ">\n");

	/* Summary */
	my_html_write_string (GTK_HTML (gtkhtml), stream, "<H1>");
	my_html_write_string (GTK_HTML (gtkhtml), stream,
			      notice->summary ?
			        notice->summary :
				_("No summary"));
	my_html_write_string (GTK_HTML (gtkhtml), stream, "</H1>\n<P>\n");

	/* If this is a dead notice */
	if (notice->state == GRAPEVINE_NOTICE_DEAD) {
		if (notice->priority == GRAPEVINE_CRITICAL)
			my_html_write_string (GTK_HTML (gtkhtml), stream,
					      "<B><FONT SIZE=+1 "
					      "COLOR=\"#000000\">");
		else
			my_html_write_string (GTK_HTML (gtkhtml), stream,
					      "<B><FONT SIZE=+1 "
					      "COLOR=\"#ff0000\">");
		
		my_html_write_string (GTK_HTML (gtkhtml), stream,
				      _("This notice has been removed!"));
		my_html_write_string (GTK_HTML (gtkhtml), stream,
				      "</FONT></B><P>\n");
	}


	/* Progress */
	if (notice->progress >= 0) {
		g_snprintf (buf, sizeof (buf), _("Progress: <B>%d%%</B><BR>\n"),
			    notice->progress);
		my_html_write_string (GTK_HTML (gtkhtml), stream, buf);
	}

	/* Priority */
	my_html_write_string (GTK_HTML (gtkhtml), stream,
			      _("Priority: <B>"));
	my_html_write_string (GTK_HTML (gtkhtml), stream,
			      _(priorities[notice->priority]));
	my_html_write_string (GTK_HTML (gtkhtml), stream, "</B><BR>\n");

	/* Timestamp */
	tm = localtime (&notice->timestamp);
	my_html_write_string (GTK_HTML (gtkhtml), stream,
			      _("Recieved at: <B>"));
	if (strftime (buf, sizeof (buf), _("%I:%M %p %a %b %d"), tm) <= 0)
		strcpy (buf, "???");
	buf[sizeof (buf)-1] = '\0';
	my_html_write_string (GTK_HTML (gtkhtml), stream, buf);
	my_html_write_string (GTK_HTML (gtkhtml), stream, "</B><BR>\n");

	/* Expires at */
	if (notice->expire_at > 0) {
		the_time = notice->expire_at;
		tm = localtime (&the_time);
		my_html_write_string (GTK_HTML (gtkhtml), stream,
				      _("Expires at: <B>"));
		if (strftime (buf, sizeof (buf),
			      _("%I:%M %p %a %b %d"), tm) <= 0)
			strcpy (buf, "???");
		buf[sizeof (buf)-1] = '\0';
		my_html_write_string (GTK_HTML (gtkhtml), stream, buf);
		my_html_write_string (GTK_HTML (gtkhtml), stream, "</B><BR>\n");
	}

	/* Source */
	my_html_write_string (GTK_HTML (gtkhtml), stream,
			      _("Source: <B>"));
	my_html_write_string (GTK_HTML (gtkhtml), stream, 
			      grp_sure_string (notice->source));
	my_html_write_string (GTK_HTML (gtkhtml), stream, "</B><BR>\n");

	/* Keywords */
	if (notice->keywords != NULL) {
		my_html_write_string (GTK_HTML (gtkhtml), stream,
				      _("Keywords: <B>"));
		s = g_strjoinv (", ", notice->keywords);
		my_html_write_string (GTK_HTML (gtkhtml), stream, s);
		g_free (s);
		my_html_write_string (GTK_HTML (gtkhtml), stream, "</B><BR>\n");
	}

	my_html_write_string (GTK_HTML (gtkhtml), stream, "<P>");

	/* The content */
	if (notice->content != NULL) {
		my_html_write_string (GTK_HTML (gtkhtml), stream,
				      notice->content);
	} else {
		my_html_write_string (GTK_HTML (gtkhtml), stream,
				      _("<B>Notice has no content!</B>"));
	}

	/* Related */
	if (notice->related != NULL) {
		GList *li;
		char *sep = "";
		my_html_write_string (GTK_HTML (gtkhtml), stream,
				      _("<P>Related to:<BR>\n"));

		for (li = notice->related; li != NULL; li = li->next) {
			GrapevineNoticeReal *rel = li->data;
			char *s;
			if (rel == notice)
				continue;

			if (rel->progress >= 0)
				s = g_strdup_printf ("%s <A HREF="
						     "\"x-notice:%s\">"
						     "%s (%d%%)</A>",
						     sep,
						     rel->uniqueID,
						     rel->summary,
						     rel->progress);
			else
				s = g_strdup_printf ("%s <A HREF="
						     "\"x-notice:%s\">"
						     "%s</A>",
						     sep,
						     rel->uniqueID,
						     rel->summary);

			my_html_write_string (GTK_HTML (gtkhtml), stream, s);
			g_free (s);

			sep = ",";
		}
	}

	/* End */
	my_html_write_string (GTK_HTML (gtkhtml), stream,
			       "</BODY>\n</HTML>\n");
	gtk_html_end (GTK_HTML (gtkhtml), stream, GTK_HTML_STREAM_OK);
}

static void
notice_changed (GrapevineNoticeReal *notice, GladeXML *xml)
{
	view_setup_view (xml, notice);
}

static void
notice_removed (GrapevineNoticeReal *notice, GladeXML *xml)
{
	view_setup_view (xml, notice);
}

static void
url_requested (GtkHTML *html, const gchar *url, GtkHTMLStream *handle,
	       GrapevineNoticeReal *notice)
{
	const char *data;
	int size;

	printf ("Requesting url: '%s'\n", url);

	data = grapevine_notice_real_peek_data (notice, url, &size);

	if (data == NULL)
		return;

	printf ("Found some data!\n");

	gtk_html_write (html, handle, data, size);

	gtk_html_end (html, handle, GTK_HTML_STREAM_OK);

	puts ("END of data!");
}

/* mostly stolen from gnome-libs */

static gint
win_hints_get_workspace (Window win)
{
	gint ws;
	Atom r_type;
	int r_format;
	unsigned long count;
	unsigned long bytes_remain;
	unsigned char *prop;
	static Atom _XA_WIN_WORKSPACE = 0;

	if (win == 0)
		return -1;

	if (_XA_WIN_WORKSPACE == 0) {
		_XA_WIN_WORKSPACE = XInternAtom (GDK_DISPLAY (),
						 XA_WIN_WORKSPACE, False);
	}

	if (XGetWindowProperty (GDK_DISPLAY (), win, _XA_WIN_WORKSPACE, 0, 1,
				False, XA_CARDINAL, &r_type, &r_format,
				&count, &bytes_remain, &prop) == Success) {
		if (r_type == XA_CARDINAL && r_format == 32 && count == 1) {
			ws = (gint)(((long *)prop)[0]); 
			XFree (prop);
			return ws;
		}
		XFree (prop);
	}

	return -1;
}

static gboolean
goto_x_window (Window wid)
{
	int ws;

	if (wid == 0)
		return FALSE;

	gdk_error_trap_push ();

	if (gnome_win_hints_wm_exists ()) {
		ws = win_hints_get_workspace (wid);

		if (ws >= 0)
			gnome_win_hints_set_current_workspace (ws);
	}

	XRaiseWindow (GDK_DISPLAY (), wid);

	gdk_flush ();
	return (gdk_error_trap_pop () == 0);
}

static void
link_clicked (GtkHTML *html, const gchar *url, gpointer data)
{
	GtkWidget *dialog = data;
	if (strncmp (url, "x-notice:", strlen ("x-notice:")) == 0) {
		const char *id = &url[strlen ("x-notice:")];
		GrapevineNoticeReal *notice =
			grapevine_main_find_notice (main_object, id);
		if (notice != NULL) {
			view_show (notice, NULL);
		} else {
			char *s;
			GtkWidget *w;

			s = g_strdup_printf (_("Notice ID: %s not found"), id);
			w = gnome_error_dialog (s);
			g_free (s);
			dialog_setup (w, GTK_WINDOW (dialog),
				      GTK_OBJECT (dialog));
		}
	} else if (strncmp (url, "x-goto-window:",
			    strlen ("x-goto-window:")) == 0) {
		gulong wid;

		wid = 0;

		if (sscanf (url, "x-goto-window:0x%lx", &wid) == 0)
			sscanf (url, "x-goto-window:%lu", &wid);

		if ( !  goto_x_window (wid)) {
			char *s;
			GtkWidget *w;

			s = g_strdup_printf (_("Problems when trying to raise "
					       "window ID: 0x%lx"),
					     wid);
			w = gnome_error_dialog (s);
			g_free (s);
			dialog_setup (w, GTK_WINDOW (dialog),
				      GTK_OBJECT (dialog));
		}
	} else {
		gnome_url_show (url);
	}
}

void
view_show (GrapevineNoticeReal *notice, GtkWindow *parent)
{
	GladeXML *xml;
	GtkWidget *dialog;
	GtkWidget *gtkhtml;
	GtkWidget *w;

	puts ("RUNNING VIEW DIALOG");

	xml = glade_helper_load ("grapevine.glade", "view_notice_dialog",
				 gnome_dialog_get_type (),
				 TRUE /* dump on destroy */);

	active_views ++;

	gtk_object_ref (GTK_OBJECT (notice));

	dialog = dialog_do_dialog (xml, "view_notice_dialog", parent,
				   GTK_OBJECT (notice));
	if (dialog == NULL)
		return;

	gtk_object_set_user_data (GTK_OBJECT (dialog), notice);
	gtk_signal_connect (GTK_OBJECT (dialog), "destroy",
			    GTK_SIGNAL_FUNC (view_dialog_destroy),
			    xml);
	gtk_signal_connect (GTK_OBJECT (dialog), "clicked",
			    GTK_SIGNAL_FUNC (view_dialog_clicked),
			    xml);

	gtkhtml = gtk_html_new ();
	gtk_object_set_data (GTK_OBJECT (dialog), "gtkhtml", gtkhtml);
	gtk_signal_connect (GTK_OBJECT (gtkhtml), "url_requested",
			    GTK_SIGNAL_FUNC (url_requested),
			    notice);
	gtk_signal_connect (GTK_OBJECT (gtkhtml), "link_clicked",
			    GTK_SIGNAL_FUNC (link_clicked), dialog);

	gtk_html_set_editable (GTK_HTML (gtkhtml), FALSE);
	gtk_widget_show (gtkhtml);

	w = glade_helper_get (xml, "view_scrolled_window",
			      GTK_TYPE_SCROLLED_WINDOW);

	gtk_container_add (GTK_CONTAINER (w), gtkhtml);

	gtk_signal_connect_after (GTK_OBJECT (notice), "removed",
				  GTK_SIGNAL_FUNC (notice_removed),
				  xml);
	gtk_signal_connect_after (GTK_OBJECT (notice), "changed",
				  GTK_SIGNAL_FUNC (notice_changed),
				  xml);

	view_setup_view (xml, notice);

	/* Play the view sound if one got set up */
	grapevine_notice_real_play_view_sound (notice);
}
