/* Grapevine: GNOME Notifications
 *   Utility functions
 * Author: George Lebl
 * (c) 2000 Eazel, Inc.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#ifndef GRAPEVINE_UTIL_H
#define GRAPEVINE_UTIL_H

#include <pong/pong.h>

/* count length of a vector */
int		grp_strlengthv				(char **argv);

/* if argc is -1, then argv is taken as a NULL terminated vector */
char **		grp_strdupv				(char ** argv,
							 int argc);

/* gets a unique ID string, each call will give a different string */
char *		grp_create_unique_id			(void);

enum {
	GRP_QUESTION_YES_NO,
	GRP_QUESTION_OK_CANCEL
};
typedef void (*GrpCallback)(gpointer);

GtkWidget *	grp_question_dialog			(const char *question,
							 int dlg_type,
							 GrpCallback yes_callback,
							 gpointer data,
							 GDestroyNotify destroy_notify,
							 const char *gconf_show_key,
							 gboolean yes_when_no_dialog,
							 GtkWindow *parent);


gboolean	grp_question_dialog_modal		(const char *question,
							 int dlg_type,
							 const char *gconf_show_key,
							 gboolean yes_when_no_dialog,
							 GtkWindow *parent);

enum {
	GRP_MSG_OK
};

GtkWidget *	grp_msg_dialog				(const char *msg,
							 int dlg_type,
							 const char *gconf_show_key,
							 GtkWindow *parent);

/* An alarm in a specified number of seconds, implemented within the
 * gtk main loop */
typedef void (*GrpAlarmFunc)(gpointer data);
gpointer	grp_alarm_add				(unsigned long alarm,
							 GrpAlarmFunc function,
							 GDestroyNotify destroy_notify,
							 gpointer data);
void		grp_alarm_remove			(gpointer handle);

void		grp_play_sound				(const char *data,
							 int length);

/* show properties dialog for file and prefix */
PongXML *	grapevine_properties_show		(const char *file,
							 const char *prefix);

/* Do strcasecmp but ignore locale, usefull for parsing where locale would
 * just do damage */
int		grp_strcasecmp_no_locale		(const char *s1,
							 const char *s2);

const char *	grp_sure_string				(const char *s);
gboolean	grp_string_empty			(const char *s);

void		grp_list_free_deep			(GList *list);
void		grp_slist_free_deep			(GSList *list);

void		set_widget_color			(GtkWidget *widget,
							 GdkColor *color);

guint		str_case_hash				(gconstpointer a);
gboolean	str_case_equal				(gconstpointer a,
							 gconstpointer b);

/* For dumb error handeling */
#define GRP_HANDLE_GCONF_ERROR(stmt)			    	                \
        do {                                                                    \
	        if (error) {						        \
		        g_warning (_("GConf error:\n  %s"), error->message);	\
  		        g_error_free (error);			                \
		        error = NULL;					        \
		        stmt ;					 	        \
	        }                                                               \
       } while (0)

#endif /* GRAPEVINE_UTIL_H */
